-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: academy_lms
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addons`
--

DROP TABLE IF EXISTS `addons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `addons` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `unique_identifier` varchar(255) NOT NULL,
  `version` varchar(255) DEFAULT NULL,
  `status` int NOT NULL,
  `created_at` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  `about` longtext,
  `purchase_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addons`
--

LOCK TABLES `addons` WRITE;
/*!40000 ALTER TABLE `addons` DISABLE KEYS */;
INSERT INTO `addons` VALUES (2,'Course Forum','forum','1.0',1,1916179200,NULL,'It opportunity the user to any questions or answers about courses.','e6ca25cb-b7ed-4fe0-b2f2-34005f4d4e1d');
/*!40000 ALTER TABLE `addons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `applications` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `address` longtext,
  `phone` varchar(255) DEFAULT NULL,
  `message` longtext,
  `document` varchar(255) DEFAULT NULL,
  `status` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applications`
--

LOCK TABLES `applications` WRITE;
/*!40000 ALTER TABLE `applications` DISABLE KEYS */;
/*!40000 ALTER TABLE `applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int DEFAULT '0',
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int DEFAULT NULL,
  `last_modified` int DEFAULT NULL,
  `font_awesome_class` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'d444465464','Cơ bản',0,'cơ-bản',1632960000,NULL,'fas fa-chess','category-thumbnail.png'),(2,'e421a4e004','Nâng cao',0,'nâng-cao',1632960000,NULL,'fas fa-chess','category-thumbnail.png'),(3,'b277c24e37','Làm quen',4,'làm-quen',1632960000,1632960000,'fas fa-chess','category-thumbnail.png'),(4,'a061f2a93b','Chung',0,'chung',1632960000,NULL,'fas fa-chess','category-thumbnail.png'),(5,'685717639e','Kiến thức về Chứng khoán',1,'kiến-thức-về-chứng-khoán',1632960000,NULL,NULL,NULL),(6,'21d85929c4','Phân tích kỹ thuật',2,'phân-tích-kỹ-thuật',1632960000,NULL,NULL,NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('9btdg2ud76jbvnn9qqbri85j55dl8rap','14.189.251.64',1632929390,_binary '__ci_last_regenerate|i:1632929390;cart_items|a:0:{}language|s:7:\"english\";'),('8vqlrvvsdlsg6nlaea4pja77s8kn0v2t','14.189.251.64',1632929390,_binary '__ci_last_regenerate|i:1632929390;cart_items|a:0:{}language|s:7:\"english\";'),('c0d9usegmpfi15pbdtr26vimdn91k4k6','14.189.251.64',1632929390,_binary '__ci_last_regenerate|i:1632929390;cart_items|a:0:{}language|s:7:\"english\";'),('2o6jelvpin7t3ufaeq53r2ifdd5lotkg','14.189.251.64',1632929390,_binary '__ci_last_regenerate|i:1632929390;cart_items|a:0:{}language|s:7:\"english\";'),('4tnel5803leda9upn1juvte5tbhi65dg','14.189.251.64',1632929390,_binary '__ci_last_regenerate|i:1632929390;cart_items|a:0:{}language|s:7:\"english\";'),('rbj36jk9kk3hrt87lp1i5ab898vvr4op','14.189.251.64',1632930615,_binary '__ci_last_regenerate|i:1632930615;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"1\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:17:\"Tran Vu  Viet Anh\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('t8kv0p2bpd4hbcud4ci2ppooro1bbl9l','14.177.210.6',1632929407,_binary '__ci_last_regenerate|i:1632929407;cart_items|a:0:{}language|s:7:\"english\";'),('k4bfm8vc07j6vn9sbp9hvtk2n7853jsg','14.177.210.6',1632929407,_binary '__ci_last_regenerate|i:1632929407;cart_items|a:0:{}language|s:7:\"english\";'),('2dkddhjei3b6unj3vajt669ij6c2330e','14.177.210.6',1632929407,_binary '__ci_last_regenerate|i:1632929407;cart_items|a:0:{}language|s:7:\"english\";'),('7b93tvcg4m3ippnhkd9f956pvjq2cl8e','14.177.210.6',1632929407,_binary '__ci_last_regenerate|i:1632929407;cart_items|a:0:{}language|s:7:\"english\";'),('3h8pph7u7oe4k8h3eur2hegfrgfh9ath','14.177.210.6',1632929408,_binary '__ci_last_regenerate|i:1632929407;cart_items|a:0:{}language|s:7:\"english\";'),('pp9m7hu9r37udjqld8ng0m1tsrpcblfc','14.177.210.6',1632929408,_binary '__ci_last_regenerate|i:1632929407;cart_items|a:0:{}language|s:7:\"english\";'),('cohhpu3fcatvf56n2bop9qvdfcq6jhaq','14.177.210.6',1632929407,_binary '__ci_last_regenerate|i:1632929407;cart_items|a:0:{}language|s:7:\"english\";'),('ga0b44qfgnniukkrc80pl8vdc8m4co8l','45.87.61.116',1632930030,_binary '__ci_last_regenerate|i:1632930030;cart_items|a:0:{}language|s:7:\"english\";'),('9jnkp5au49lv9lkall58900pdd2hp10p','45.87.61.116',1632930030,_binary '__ci_last_regenerate|i:1632930030;cart_items|a:0:{}language|s:7:\"english\";'),('hbgi7hsqau3m1q5nfq8pq8fv9m684i63','45.87.61.116',1632930031,_binary '__ci_last_regenerate|i:1632930031;cart_items|a:0:{}language|s:7:\"english\";'),('4ogpfi3qqbejq3apabg7conl5m8jgdn6','45.87.61.116',1632930032,_binary '__ci_last_regenerate|i:1632930032;cart_items|a:0:{}language|s:7:\"english\";'),('bue2cradvjc5ojequdjmphufr1bjf93p','14.189.251.64',1632931285,_binary '__ci_last_regenerate|i:1632931285;cart_items|a:0:{}language|s:7:\"english\";'),('sl4q6o8r40tjh6v87470fpiv2v6md4ig','162.142.125.41',1632930898,_binary '__ci_last_regenerate|i:1632930898;cart_items|a:0:{}language|s:7:\"english\";'),('d2i7ge14m65rm902j2315peno66pj1ta','162.142.125.41',1632930899,_binary '__ci_last_regenerate|i:1632930899;cart_items|a:0:{}language|s:7:\"english\";'),('a4c3g5rfkbb9o13bio5d5jp1rr9ckov0','192.162.245.80',1632931027,_binary '__ci_last_regenerate|i:1632931027;cart_items|a:0:{}language|s:7:\"english\";'),('onsk1jh6n2ackjb4q871kujl20nluf5n','14.189.251.64',1632932292,_binary '__ci_last_regenerate|i:1632931285;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"1\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:17:\"Tran Vu  Viet Anh\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('1mfs6hg2mlk4qqjcbhninnq8nqqgot6k','107.189.14.98',1632931339,_binary '__ci_last_regenerate|i:1632931339;cart_items|a:0:{}language|s:7:\"english\";'),('82c70nute18rrrqna563754845gd5d7f','201.217.152.162',1632932481,_binary '__ci_last_regenerate|i:1632932481;cart_items|a:0:{}language|s:7:\"english\";'),('o6es0eku1rd6pu4ct22d15tjab0djhev','192.162.245.80',1632934621,_binary '__ci_last_regenerate|i:1632934621;cart_items|a:0:{}language|s:7:\"english\";'),('5usmgelmb98rhi14d6jnmpuabeotjidd','212.83.141.16',1632935324,_binary '__ci_last_regenerate|i:1632935324;cart_items|a:0:{}language|s:7:\"english\";'),('9l0qgsbf89e906bjokn9qd9ildcd55hl','212.83.141.16',1632935324,_binary '__ci_last_regenerate|i:1632935324;cart_items|a:0:{}language|s:7:\"english\";'),('01s7i8s5o6m1l89c6t315oplubelg4cr','212.83.141.16',1632935326,_binary '__ci_last_regenerate|i:1632935326;cart_items|a:0:{}language|s:7:\"english\";'),('qta5ejkl4n31sj3kggfhe0l3v4oiiequ','212.83.141.16',1632935327,_binary '__ci_last_regenerate|i:1632935327;cart_items|a:0:{}language|s:7:\"english\";'),('ksjj4up9m8sjjoircaami1a98gfrbdtl','212.83.141.16',1632935328,_binary '__ci_last_regenerate|i:1632935328;cart_items|a:0:{}language|s:7:\"english\";'),('dkse5ve3edtd1odq2b0bjl0jdoschmjg','212.83.141.16',1632935329,_binary '__ci_last_regenerate|i:1632935329;cart_items|a:0:{}language|s:7:\"english\";'),('7fe2miuhr9fslipaoiloir5varjsh1co','212.83.141.16',1632935330,_binary '__ci_last_regenerate|i:1632935330;cart_items|a:0:{}language|s:7:\"english\";'),('ftrrjfdhc5r16gmod7vev787hjrl72id','212.83.141.16',1632935331,_binary '__ci_last_regenerate|i:1632935331;cart_items|a:0:{}language|s:7:\"english\";'),('esuva58ruiml1rkgccgts9gqmmvq8gur','212.83.141.16',1632935332,_binary '__ci_last_regenerate|i:1632935332;cart_items|a:0:{}language|s:7:\"english\";'),('lnt6bqbo20go3vdl219vs58d2ahc28g3','212.83.141.16',1632935334,_binary '__ci_last_regenerate|i:1632935334;cart_items|a:0:{}language|s:7:\"english\";'),('v1doqc2pvto1nus03dbfcag4a1asqt84','212.83.141.16',1632935334,_binary '__ci_last_regenerate|i:1632935334;cart_items|a:0:{}language|s:7:\"english\";'),('igliiiq7tri98i4cbssh39og7937790j','212.83.141.16',1632935336,_binary '__ci_last_regenerate|i:1632935336;cart_items|a:0:{}language|s:7:\"english\";'),('f5umpakdlqit0j9556hbh0ef2s64bgc3','212.83.141.16',1632935336,_binary '__ci_last_regenerate|i:1632935336;cart_items|a:0:{}language|s:7:\"english\";'),('2nhejie54m7ff11ado7b7oiloa89addq','212.83.141.16',1632935338,_binary '__ci_last_regenerate|i:1632935338;cart_items|a:0:{}language|s:7:\"english\";'),('qcg4a62vtb57h984gfdnrenvb713aotp','212.83.141.16',1632935339,_binary '__ci_last_regenerate|i:1632935339;cart_items|a:0:{}language|s:7:\"english\";'),('e2gplg1jk4i46e30m93spge5ip7med89','212.83.141.16',1632935340,_binary '__ci_last_regenerate|i:1632935340;cart_items|a:0:{}language|s:7:\"english\";'),('nhsq9dv63gk9d5r7d4pta540u7oke7nd','212.83.141.16',1632935341,_binary '__ci_last_regenerate|i:1632935341;cart_items|a:0:{}language|s:7:\"english\";'),('l6g0n739n98ufohjh8137b9jdmf09548','184.105.139.68',1632935675,_binary '__ci_last_regenerate|i:1632935675;cart_items|a:0:{}language|s:7:\"english\";'),('6t8oovarni68v2g5oumsc7qesr5kgib8','45.95.147.14',1632936252,_binary '__ci_last_regenerate|i:1632936252;cart_items|a:0:{}language|s:7:\"english\";'),('1obusgraacas2jr95ku09t28ufl9ej97','128.14.133.58',1632936790,_binary '__ci_last_regenerate|i:1632936790;cart_items|a:0:{}language|s:7:\"english\";'),('qcbgk4c75gir6nchslgpaqhvmichhg2g','107.189.6.44',1632937679,_binary '__ci_last_regenerate|i:1632937679;cart_items|a:0:{}language|s:7:\"english\";'),('6ah4gsalmu3k7nkquh26uqmvfi16p5d3','192.162.245.80',1632938455,_binary '__ci_last_regenerate|i:1632938455;cart_items|a:0:{}language|s:7:\"english\";'),('o93gc6mqpdbvluhpn7jl5fqmqjvu1no9','54.169.116.244',1632939456,_binary '__ci_last_regenerate|i:1632939456;cart_items|a:0:{}language|s:7:\"english\";'),('dgihj7d14mj26q4b3d9ek26lak4m9t9h','27.198.147.230',1632943213,_binary '__ci_last_regenerate|i:1632943213;cart_items|a:0:{}language|s:7:\"english\";'),('a6tjplenk114vk6sf9fvkeu34q5thetf','80.82.78.39',1632943878,_binary '__ci_last_regenerate|i:1632943878;cart_items|a:0:{}language|s:7:\"english\";'),('s81g267ui6mkjau6ree72asuvige0k9v','167.248.133.42',1632944659,_binary '__ci_last_regenerate|i:1632944659;cart_items|a:0:{}language|s:7:\"english\";'),('a6kdcndomdm4ctbfpmg2cetl3n33eo38','167.248.133.42',1632944660,_binary '__ci_last_regenerate|i:1632944660;cart_items|a:0:{}language|s:7:\"english\";'),('5u8hn4e7obtovfg2q17if6indf1t3j0u','107.189.14.98',1632945431,_binary '__ci_last_regenerate|i:1632945431;cart_items|a:0:{}language|s:7:\"english\";'),('bb7dht7u46oo75rvfdqbhsuj66g3qdo0','109.248.6.101',1632945890,_binary '__ci_last_regenerate|i:1632945890;cart_items|a:0:{}language|s:7:\"english\";'),('u0mvel9ad8hvtogte9qfdl3tr19taa2k','107.180.88.41',1632945946,_binary '__ci_last_regenerate|i:1632945946;cart_items|a:0:{}language|s:7:\"english\";'),('mume5sqoroovdu6re3h8g973hifk23ac','209.141.43.209',1632947632,_binary '__ci_last_regenerate|i:1632947632;cart_items|a:0:{}language|s:7:\"english\";'),('92erh6fu7bnoi3b5cgtg332sf4hk55h1','54.36.148.83',1632947862,_binary '__ci_last_regenerate|i:1632947862;cart_items|a:0:{}language|s:7:\"english\";'),('vnhr3396sjvd09dvcqs76cl34lmoefsk','107.189.6.44',1632949054,_binary '__ci_last_regenerate|i:1632949054;cart_items|a:0:{}language|s:7:\"english\";'),('0m0t6no394hk1c3hplf0hcjr6ingtnme','192.162.245.80',1632949612,_binary '__ci_last_regenerate|i:1632949612;cart_items|a:0:{}language|s:7:\"english\";'),('qd6emnffqu6k7h6asdquqke9dscrpv9h','54.221.27.173',1632949726,_binary '__ci_last_regenerate|i:1632949726;cart_items|a:0:{}language|s:7:\"english\";'),('pk5hp8rnh1vqmtls1r5obba5umh3hujv','151.235.240.6',1632951358,_binary '__ci_last_regenerate|i:1632951358;cart_items|a:0:{}language|s:7:\"english\";'),('tviop0mjb9p4kcc8dqqjqm19o40n1o1r','212.83.141.16',1632955304,_binary '__ci_last_regenerate|i:1632955304;cart_items|a:0:{}language|s:7:\"english\";'),('aokq8jnic65urp8kss2ghcm89i85l8t5','212.83.141.16',1632955305,_binary '__ci_last_regenerate|i:1632955305;cart_items|a:0:{}language|s:7:\"english\";'),('87t9dj604i1t3hakod0a6b6g4mokcple','212.83.141.16',1632955306,_binary '__ci_last_regenerate|i:1632955306;cart_items|a:0:{}language|s:7:\"english\";'),('qgufmt23oqp2sf8v99guegkokokn1rek','212.83.141.16',1632955307,_binary '__ci_last_regenerate|i:1632955307;cart_items|a:0:{}language|s:7:\"english\";'),('o1brevtik3lnt0aingsjj9tm4ddi18ap','212.83.141.16',1632955308,_binary '__ci_last_regenerate|i:1632955308;cart_items|a:0:{}language|s:7:\"english\";'),('vbcq75ssdduft55ce1pbiddspdeec4gg','212.83.141.16',1632955308,_binary '__ci_last_regenerate|i:1632955308;cart_items|a:0:{}language|s:7:\"english\";'),('estok9ue4te6f0nelp13r2e7rdq8ct4l','212.83.141.16',1632955309,_binary '__ci_last_regenerate|i:1632955309;cart_items|a:0:{}language|s:7:\"english\";'),('pqu5k5v7jmrelr26mlpqofobfgo89bgs','212.83.141.16',1632955310,_binary '__ci_last_regenerate|i:1632955310;cart_items|a:0:{}language|s:7:\"english\";'),('5lm78d2jhorlq6p7drr4gdu208ka97v0','212.83.141.16',1632955311,_binary '__ci_last_regenerate|i:1632955311;cart_items|a:0:{}language|s:7:\"english\";'),('fnpsg7fg3n4001bqqcraftj31a9c4lj1','212.83.141.16',1632955313,_binary '__ci_last_regenerate|i:1632955313;cart_items|a:0:{}language|s:7:\"english\";'),('dlreu97l61p6lrbft631c9nfk2hckf6h','212.83.141.16',1632955313,_binary '__ci_last_regenerate|i:1632955313;cart_items|a:0:{}language|s:7:\"english\";'),('bfeitn5q6mv9nbv6n5hochsuipn6t4rk','212.83.141.16',1632955314,_binary '__ci_last_regenerate|i:1632955314;cart_items|a:0:{}language|s:7:\"english\";'),('p64tgno9bk08u3v68uuiuvqgt5op2crc','212.83.141.16',1632955315,_binary '__ci_last_regenerate|i:1632955315;cart_items|a:0:{}language|s:7:\"english\";'),('9f2eta3ppfsko7vi3ktervr4c5b6s54o','212.83.141.16',1632955316,_binary '__ci_last_regenerate|i:1632955316;cart_items|a:0:{}language|s:7:\"english\";'),('h117esr9ev95bggq9nbr7r45gmer2rdq','212.83.141.16',1632955317,_binary '__ci_last_regenerate|i:1632955317;cart_items|a:0:{}language|s:7:\"english\";'),('cr9phsjsd0ud70lon6oo1i5083bia0hp','212.83.141.16',1632955318,_binary '__ci_last_regenerate|i:1632955318;cart_items|a:0:{}language|s:7:\"english\";'),('fhivccb97taonuvkm0ke8104vppf95ro','212.83.141.16',1632955319,_binary '__ci_last_regenerate|i:1632955319;cart_items|a:0:{}language|s:7:\"english\";'),('msh5dffaaa9kvp1a7quenps22s6uvsu7','31.44.185.115',1632956080,_binary '__ci_last_regenerate|i:1632956080;cart_items|a:0:{}language|s:7:\"english\";'),('qnn11r0i25mpuf4hvt265cjjdtsle19g','46.236.191.74',1632957007,_binary '__ci_last_regenerate|i:1632957007;cart_items|a:0:{}language|s:7:\"english\";'),('jgjg9rjskfv6tj6tpo26c49hu67s8t8h','45.146.164.110',1632958061,_binary '__ci_last_regenerate|i:1632958061;cart_items|a:0:{}language|s:7:\"english\";'),('gb9f6vkh4rtrnifcvi71935etlcqvre0','45.146.164.110',1632958063,_binary '__ci_last_regenerate|i:1632958063;cart_items|a:0:{}language|s:7:\"english\";'),('pu9sr9v30kd0p48nl3o2ng1dn66ccsig','45.146.164.110',1632958064,_binary '__ci_last_regenerate|i:1632958064;cart_items|a:0:{}language|s:7:\"english\";'),('9t2quc99n4pqkf1a6qsi588c3ag78hkn','45.146.164.110',1632958069,_binary '__ci_last_regenerate|i:1632958069;cart_items|a:0:{}language|s:7:\"english\";'),('laf1ql3en4vbesetfcstk65rof9jgkm6','45.146.164.110',1632958073,_binary '__ci_last_regenerate|i:1632958073;cart_items|a:0:{}language|s:7:\"english\";'),('njthb4oq496hvbmv4v38d4j24ccnbje1','45.146.164.110',1632958075,_binary '__ci_last_regenerate|i:1632958075;cart_items|a:0:{}language|s:7:\"english\";'),('a2ukd6ujbm016r0crlrk1f0j9va1p8s3','45.146.164.110',1632958077,_binary '__ci_last_regenerate|i:1632958077;cart_items|a:0:{}language|s:7:\"english\";'),('260tsr4sti9nknlv3o4hp2s3o0td8mhm','45.146.164.110',1632958079,_binary '__ci_last_regenerate|i:1632958079;cart_items|a:0:{}language|s:7:\"english\";'),('63fc93esbhoghfs1gv6edpjh520gv1fh','46.39.238.106',1632959651,_binary '__ci_last_regenerate|i:1632959651;cart_items|a:0:{}language|s:7:\"english\";'),('mu0ep8j7fr8h7jgkq01asskkpp1r6gkl','107.189.14.98',1632959776,_binary '__ci_last_regenerate|i:1632959776;cart_items|a:0:{}language|s:7:\"english\";'),('jvh4iori3f02a4tfkcar9f95hufnaap4','192.241.201.135',1632960227,_binary '__ci_last_regenerate|i:1632960227;cart_items|a:0:{}language|s:7:\"english\";'),('dugf7gp3cd4qr175u40i6d248kvk09kg','107.189.6.44',1632961002,_binary '__ci_last_regenerate|i:1632961001;cart_items|a:0:{}language|s:7:\"english\";'),('t0l3ju3fu88k85bdiruu5ro02i1fjr4k','109.195.103.59',1632962516,_binary '__ci_last_regenerate|i:1632962516;cart_items|a:0:{}language|s:7:\"english\";'),('86lknul83o93veog5tpddvccmmaot08k','198.98.59.45',1632962918,_binary '__ci_last_regenerate|i:1632962918;cart_items|a:0:{}language|s:7:\"english\";'),('iaq5hr27rt115b2ue34hiduqr7pip2ju','198.98.59.45',1632963105,_binary '__ci_last_regenerate|i:1632963105;cart_items|a:0:{}language|s:7:\"english\";total_price_of_checking_out|i:0;'),('mbt999et59ng3tr25edt2g8bjroi4g79','198.98.59.45',1632963192,_binary '__ci_last_regenerate|i:1632963192;cart_items|a:0:{}language|s:7:\"english\";'),('trnjm3p18ngmtf65rhl7kgc1qc1jhan8','66.249.64.175',1632963760,_binary '__ci_last_regenerate|i:1632963760;cart_items|a:0:{}language|s:7:\"english\";'),('gemqf0t4o8vaj3vt2l59eq30tagjf92a','14.189.251.64',1632964580,_binary '__ci_last_regenerate|i:1632964580;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"1\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:17:\"Tran Vu  Viet Anh\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('jq2r0io062vesd473j79ncl74dceujus','3.239.206.109',1632964263,_binary '__ci_last_regenerate|i:1632964263;cart_items|a:0:{}language|s:7:\"english\";'),('d2r63abti5mi61l3dmld8mfasf4vu195','14.189.251.64',1632970073,_binary '__ci_last_regenerate|i:1632970073;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"2\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:13:\"Quyet Dao Van\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('22lovprm669bamkr1flt20vdt93cm4fd','66.249.64.173',1632964589,_binary '__ci_last_regenerate|i:1632964588;cart_items|a:0:{}language|s:7:\"english\";'),('lb4k7d7vfetb6sg854pf6712h96irdub','66.249.64.175',1632964621,_binary '__ci_last_regenerate|i:1632964621;cart_items|a:0:{}language|s:7:\"english\";'),('0n3j0audlhgrgifmfbbfs95jsqm8vpc8','198.98.59.45',1632964622,_binary '__ci_last_regenerate|i:1632964622;cart_items|a:0:{}language|s:7:\"english\";'),('2uevd86lund9h7bsi649obiarqklp91g','198.98.59.45',1632964754,_binary '__ci_last_regenerate|i:1632964754;cart_items|a:0:{}language|s:7:\"english\";total_price_of_checking_out|i:0;'),('qnf22kdn6fin3pps02q017gjd5cm8pkh','198.98.59.45',1632964812,_binary '__ci_last_regenerate|i:1632964812;cart_items|a:0:{}language|s:7:\"english\";'),('ho1bc0v7h4mmesq16tg5ms5ln7nc0ktu','117.6.134.241',1632965076,_binary '__ci_last_regenerate|i:1632965048;cart_items|a:0:{}language|s:7:\"english\";'),('j0s6h5nhtmjm9rj4m9bh9ii21icctav8','117.6.134.241',1632965457,_binary '__ci_last_regenerate|i:1632965457;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"1\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:17:\"Tran Vu  Viet Anh\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('ff512a66kcg55p9rcjpeg79e1k5o3hi6','117.6.134.241',1632965766,_binary '__ci_last_regenerate|i:1632965766;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"1\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:17:\"Tran Vu  Viet Anh\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";total_price_of_checking_out|i:0;'),('afn910cq6fkljhld3h3mr6qcvr8s8j1h','117.6.134.241',1632966077,_binary '__ci_last_regenerate|i:1632966077;cart_items|a:0:{}language|s:7:\"english\";total_price_of_checking_out|i:0;'),('vk3m2j435121eglscg98pfp1helmt8d5','117.6.134.241',1632966437,_binary '__ci_last_regenerate|i:1632966437;cart_items|a:0:{}language|s:7:\"english\";total_price_of_checking_out|i:0;user_id|s:1:\"4\";role_id|s:1:\"2\";role|s:4:\"User\";name|s:10:\"Việt Anh\";is_instructor|s:1:\"1\";user_login|s:1:\"1\";'),('807thsol9jqbt51soj7tkos52arnd4ah','8.47.15.84',1632966226,_binary '__ci_last_regenerate|i:1632966163;cart_items|a:0:{}language|s:7:\"english\";'),('090vofdr6vnpo0eqkmcu8dvq0eqr8mkd','113.190.233.231',1632966165,_binary '__ci_last_regenerate|i:1632966165;cart_items|a:0:{}language|s:7:\"english\";'),('cn8rkk3l1ahsu8moqpncv1pb8fkigt0t','113.190.233.231',1632966165,_binary '__ci_last_regenerate|i:1632966165;cart_items|a:0:{}language|s:7:\"english\";'),('hkcgv2jl3tv3vnrem4jee3n0ftvvraum','113.190.233.231',1632966224,_binary '__ci_last_regenerate|i:1632966168;cart_items|a:0:{}language|s:7:\"english\";layout|s:4:\"list\";total_price_of_checking_out|i:0;'),('88gpp5bfuo3lui54nj1l8ab3r2tjmh5b','117.6.134.241',1632966756,_binary '__ci_last_regenerate|i:1632966756;cart_items|a:0:{}language|s:7:\"english\";'),('b30ld66410g9h5du2b00on4o201cf9fj','117.6.134.241',1632966437,_binary '__ci_last_regenerate|i:1632966437;cart_items|a:0:{}language|s:7:\"english\";total_price_of_checking_out|i:0;user_id|s:1:\"4\";role_id|s:1:\"2\";role|s:4:\"User\";name|s:10:\"Việt Anh\";is_instructor|s:1:\"1\";user_login|s:1:\"1\";'),('idjvc1obcg9kucle58c8gu6bhbmbig61','117.6.134.241',1632968980,_binary '__ci_last_regenerate|i:1632968980;cart_items|a:0:{}language|s:7:\"english\";'),('ksd6kk63lptc211a66rnte29l4gadril','34.205.90.44',1632966773,_binary '__ci_last_regenerate|i:1632966773;cart_items|a:0:{}language|s:7:\"english\";'),('o9karc8vpi0fhjcgki7e96g141m30mk4','14.189.251.64',1632968570,_binary '__ci_last_regenerate|i:1632968570;cart_items|a:0:{}language|s:7:\"english\";'),('1dd44119ad1gadnpaaesjvo3fp9gmsjv','198.98.59.45',1632966956,_binary '__ci_last_regenerate|i:1632966956;cart_items|a:0:{}language|s:7:\"english\";'),('q9hadie3v8l3esilv3liqna0acu4pl3e','198.98.59.45',1632967104,_binary '__ci_last_regenerate|i:1632967104;cart_items|a:0:{}language|s:7:\"english\";total_price_of_checking_out|i:0;'),('n8n0ruipivdcki3v6so6mklpoonq6r5r','198.98.59.45',1632967168,_binary '__ci_last_regenerate|i:1632967168;cart_items|a:0:{}language|s:7:\"english\";'),('19qbqohcdngimqi61phsuih7tcmt0ph5','139.99.122.87',1632967585,_binary '__ci_last_regenerate|i:1632967585;cart_items|a:0:{}language|s:7:\"english\";'),('mb6u71gkgoujj1m0tdep775j8lt3rso5','40.76.212.168',1632967633,_binary '__ci_last_regenerate|i:1632967633;cart_items|a:0:{}language|s:7:\"english\";'),('tpnm3ie8aaqjgmhhnc1bg8uo9fkrmttg','40.76.212.168',1632967634,_binary '__ci_last_regenerate|i:1632967634;cart_items|a:0:{}language|s:7:\"english\";'),('95tiucv16kkji2sb0l7l4pi23o4kphgv','40.76.212.168',1632967637,_binary '__ci_last_regenerate|i:1632967637;cart_items|a:0:{}language|s:7:\"english\";'),('tcrj6mtt4iuo9igrmc8lop4ia2gugplc','40.76.212.168',1632967639,_binary '__ci_last_regenerate|i:1632967639;cart_items|a:0:{}language|s:7:\"english\";'),('dd3jfoopi9qa9bo7vdr8umf3l2d4ap7m','3.8.120.34',1632968259,_binary '__ci_last_regenerate|i:1632968259;cart_items|a:0:{}language|s:7:\"english\";'),('9vb5891e3drotbfokg0pcqifr11ooni2','14.189.251.64',1632969690,_binary '__ci_last_regenerate|i:1632969690;cart_items|a:0:{}language|s:7:\"english\";'),('56pg9ucori29hth6nenb6pn3jsdmic0c','139.99.121.234',1632968971,_binary '__ci_last_regenerate|i:1632968971;cart_items|a:0:{}language|s:7:\"english\";'),('2clg3niv6p7icosha6q8jjhjnfls8frn','117.6.134.241',1632968984,_binary '__ci_last_regenerate|i:1632968980;cart_items|a:0:{}language|s:7:\"english\";'),('ag9uqoll0qp1ak97bj04ufot4kksn3v5','185.67.34.1',1632969023,_binary '__ci_last_regenerate|i:1632969023;cart_items|a:0:{}language|s:7:\"english\";'),('69hc76rv3eaheaq3l9c8f8aelska8n1r','185.67.34.1',1632969024,_binary '__ci_last_regenerate|i:1632969024;cart_items|a:0:{}language|s:7:\"english\";'),('qp2qif89fba896t304gshc44qcr6ocj9','3.238.149.242',1632969038,_binary '__ci_last_regenerate|i:1632969038;cart_items|a:0:{}language|s:7:\"english\";'),('9a2eecob3foh19qk4jio6iljannqb60l','107.189.14.98',1632969242,_binary '__ci_last_regenerate|i:1632969242;cart_items|a:0:{}language|s:7:\"english\";'),('ur86c0inqrl9fn6i0nfek8d86bkv7lv3','198.98.59.45',1632969567,_binary '__ci_last_regenerate|i:1632969567;cart_items|a:0:{}language|s:7:\"english\";'),('nfcerr35r931ronfcf6qpsitf7m0tu71','3.8.120.34',1632969622,_binary '__ci_last_regenerate|i:1632969622;cart_items|a:0:{}language|s:7:\"english\";'),('j1fruo4hahetglcplkom2255o3hrrnkr','14.189.251.64',1632970005,_binary '__ci_last_regenerate|i:1632970005;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"2\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:13:\"Quyet Dao Van\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('3aiu3fdlk3fan6ph5v1d57hs01mbgee0','198.98.59.45',1632969750,_binary '__ci_last_regenerate|i:1632969750;cart_items|a:0:{}language|s:7:\"english\";total_price_of_checking_out|i:0;'),('5cln1an3h2i6eq1kt1cu87gkf5j4vk8c','27.72.81.234',1632969886,_binary '__ci_last_regenerate|i:1632969777;cart_items|a:0:{}language|s:7:\"english\";layout|s:4:\"list\";'),('0ftq7pv2hfsbo5vrrlpi79q6jk4n5dmq','198.98.59.45',1632969856,_binary '__ci_last_regenerate|i:1632969856;cart_items|a:0:{}language|s:7:\"english\";'),('t01mcf5s14gt2dbovgpkdcjrt143gj03','117.6.134.241',1632970300,_binary '__ci_last_regenerate|i:1632970300;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"1\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:17:\"Tran Vu  Viet Anh\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('kb67p58s637b20er5am3df1efmf01kg1','89.248.160.193',1632969966,_binary '__ci_last_regenerate|i:1632969966;cart_items|a:0:{}language|s:7:\"english\";'),('ck1f5tue4svls2aujtqfv3g63btm0c3e','14.189.251.64',1632970327,_binary '__ci_last_regenerate|i:1632970327;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"2\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:13:\"Quyet Dao Van\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('sdsrv8trmsd4hs6fsjm45jotqtnngivo','14.189.251.64',1632970139,_binary '__ci_last_regenerate|i:1632970073;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"2\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:13:\"Quyet Dao Van\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('mtbbgkquaf4olda6psqj2ou6ru4hgskv','27.72.81.234',1632970231,_binary '__ci_last_regenerate|i:1632970231;cart_items|a:0:{}language|s:7:\"english\";'),('9n01rlkdcffnkfuqqk8rcdorkgpl7mt7','117.6.134.241',1632970875,_binary '__ci_last_regenerate|i:1632970875;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"1\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:17:\"Tran Vu  Viet Anh\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('f4ahhvrf98o0of00hgnudgv518co2un3','14.189.251.64',1632970638,_binary '__ci_last_regenerate|i:1632970638;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"2\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:13:\"Quyet Dao Van\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('7bb1p8g11q852lce5uvsk7ikjmuf9qqk','5.165.94.231',1632970459,_binary '__ci_last_regenerate|i:1632970459;cart_items|a:0:{}language|s:7:\"english\";'),('53b698gbf8egnu7icoi9u4mocackttro','201.217.152.162',1632970463,_binary '__ci_last_regenerate|i:1632970463;cart_items|a:0:{}language|s:7:\"english\";'),('112fgeshoj3q36nj0o2k6hsn9u2hpo4t','35.237.4.214',1632970568,_binary '__ci_last_regenerate|i:1632970568;cart_items|a:0:{}language|s:7:\"english\";'),('dfjb0k3i70a0292d9jr9dskth044hml8','66.249.70.46',1632970581,_binary '__ci_last_regenerate|i:1632970581;cart_items|a:0:{}language|s:7:\"english\";'),('s6c5ajr5ujfkhf0kmpuoidtjprup0v2k','14.189.251.64',1632971112,_binary '__ci_last_regenerate|i:1632971112;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"2\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:13:\"Quyet Dao Van\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('24q4am5cn0eo4s6on3h9prq0eiiqo0hb','27.72.81.234',1632970818,_binary '__ci_last_regenerate|i:1632970781;cart_items|a:0:{}language|s:7:\"english\";'),('dfrre14157bc8gi5qhjls6qj62msvg21','117.6.134.241',1632971664,_binary '__ci_last_regenerate|i:1632971664;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"1\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:17:\"Tran Vu  Viet Anh\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('jgspa2534sioc3pm7hn95icpt0ni587n','3.8.120.34',1632970986,_binary '__ci_last_regenerate|i:1632970986;cart_items|a:0:{}language|s:7:\"english\";'),('eug1s4j7gpmtpufaji14v4uistiln7m3','14.189.251.64',1632972070,_binary '__ci_last_regenerate|i:1632972070;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"2\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:13:\"Quyet Dao Van\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";layout|s:4:\"list\";'),('58t7q00ndu10n4c3g83tmnfs4dme3hsi','66.249.70.46',1632971191,_binary '__ci_last_regenerate|i:1632971191;cart_items|a:1:{i:0;N;}language|s:7:\"english\";'),('fkq05fgmnt9pcrdjrjreuefrqa0d6m7c','66.249.70.42',1632971201,_binary '__ci_last_regenerate|i:1632971200;cart_items|a:0:{}language|s:7:\"english\";'),('sa2n099fu4smvj0l77jndb7sjn9ukjtn','66.249.70.42',1632971211,_binary '__ci_last_regenerate|i:1632971211;cart_items|a:0:{}language|s:7:\"english\";'),('d7ljjhhgg3r3ddvskd54sbm5j6fndj8f','66.249.70.44',1632971222,_binary '__ci_last_regenerate|i:1632971222;cart_items|a:0:{}language|s:7:\"english\";'),('07anldn4uku0kt95jlh3epk7943uv2a0','3.8.120.34',1632971307,_binary '__ci_last_regenerate|i:1632971307;cart_items|a:0:{}language|s:7:\"english\";'),('kct3nni05fg7k3asunamstia96037ak9','117.6.134.241',1632971979,_binary '__ci_last_regenerate|i:1632971979;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"1\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:17:\"Tran Vu  Viet Anh\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";'),('sla3ktsvn1nfhb63762g71r6c4leheus','117.6.134.241',1632972222,_binary '__ci_last_regenerate|i:1632971979;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"1\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:17:\"Tran Vu  Viet Anh\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";url_history|s:118:\"https://daotao.quatmanhvao.com/home/course/l%C3%A0m-quen-v%E1%BB%9Bi-trang-%C4%90%C3%A0o-t%E1%BA%A1o-c%E1%BB%A7a-qmv/1\";'),('vjrph1tbp07rdr48b1qsbo4q2p23mtc0','192.241.208.203',1632972065,_binary '__ci_last_regenerate|i:1632972065;cart_items|a:0:{}language|s:7:\"english\";'),('b7nfpk66ohdl6hgrste1q9ob13qlnmb4','14.189.251.64',1632972141,_binary '__ci_last_regenerate|i:1632972070;cart_items|a:0:{}language|s:7:\"english\";user_id|s:1:\"2\";role_id|s:1:\"1\";role|s:5:\"Admin\";name|s:13:\"Quyet Dao Van\";is_instructor|s:1:\"1\";admin_login|s:1:\"1\";layout|s:4:\"list\";'),('svpbjj2bhd2b2hhf2mkomscqos9hntct','198.98.59.45',1632972259,_binary '__ci_last_regenerate|i:1632972259;cart_items|a:0:{}language|s:7:\"english\";'),('tlm068gje0tj17mpg6hn1m3dhr4odv8m','198.98.59.45',1632972393,_binary '__ci_last_regenerate|i:1632972393;cart_items|a:0:{}language|s:7:\"english\";total_price_of_checking_out|i:0;'),('g1mba8gihbfo5td6bgpm4t2c92513664','198.98.59.45',1632972467,_binary '__ci_last_regenerate|i:1632972467;cart_items|a:0:{}language|s:7:\"english\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `body` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `user_id` int DEFAULT NULL,
  `commentable_id` int DEFAULT NULL,
  `commentable_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int DEFAULT NULL,
  `last_modified` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `coupons` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount_percentage` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `expiry_date` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `course` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `outcomes` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `language` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `sub_category_id` int DEFAULT NULL,
  `section` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `requirements` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `price` double DEFAULT NULL,
  `discount_flag` int DEFAULT '0',
  `discounted_price` int DEFAULT NULL,
  `level` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int DEFAULT NULL,
  `last_modified` int DEFAULT NULL,
  `course_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_top_course` int DEFAULT '0',
  `is_admin` int DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_overview_provider` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `meta_description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `is_free_course` int DEFAULT NULL,
  `multi_instructor` int NOT NULL DEFAULT '0',
  `creator` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'Làm quen với Trang Đào tạo của QMV','Làm quen với Trang Đào tạo của QMV - Làm giàu kiến thức','<p>Làm quen với Trang Đào tạo của QMV - Làm giàu kiến thức<br></p>','[]','english',4,3,'[]','[]',0,NULL,0,'beginner','1',NULL,'',1632960000,NULL,'general',NULL,1,'active','','','',1,0,1);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_forum`
--

DROP TABLE IF EXISTS `course_forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `course_forum` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `course_id` int DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` longtext,
  `upvoted_user_id` longtext,
  `is_parent` int DEFAULT '0',
  `date_added` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_forum`
--

LOCK TABLES `course_forum` WRITE;
/*!40000 ALTER TABLE `course_forum` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_forum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `currency` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `symbol` varchar(255) DEFAULT NULL,
  `paypal_supported` int DEFAULT NULL,
  `stripe_supported` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` VALUES (1,'Leke','ALL','Lek',0,1),(2,'Dollars','USD','$',1,1),(3,'Afghanis','AFN','؋',0,1),(4,'Pesos','ARS','$',0,1),(5,'Guilders','AWG','ƒ',0,1),(6,'Dollars','AUD','$',1,1),(7,'New Manats','AZN','ман',0,1),(8,'Dollars','BSD','$',0,1),(9,'Dollars','BBD','$',0,1),(10,'Rubles','BYR','p.',0,0),(11,'Euro','EUR','€',1,1),(12,'Dollars','BZD','BZ$',0,1),(13,'Dollars','BMD','$',0,1),(14,'Bolivianos','BOB','$b',0,1),(15,'Convertible Marka','BAM','KM',0,1),(16,'Pula','BWP','P',0,1),(17,'Leva','BGN','лв',0,1),(18,'Reais','BRL','R$',1,1),(19,'Pounds','GBP','£',1,1),(20,'Dollars','BND','$',0,1),(21,'Riels','KHR','៛',0,1),(22,'Dollars','CAD','$',1,1),(23,'Dollars','KYD','$',0,1),(24,'Pesos','CLP','$',0,1),(25,'Yuan Renminbi','CNY','¥',0,1),(26,'Pesos','COP','$',0,1),(27,'Colón','CRC','₡',0,1),(28,'Kuna','HRK','kn',0,1),(29,'Pesos','CUP','₱',0,0),(30,'Koruny','CZK','Kč',1,1),(31,'Kroner','DKK','kr',1,1),(32,'Pesos','DOP ','RD$',0,1),(33,'Dollars','XCD','$',0,1),(34,'Pounds','EGP','£',0,1),(35,'Colones','SVC','$',0,0),(36,'Pounds','FKP','£',0,1),(37,'Dollars','FJD','$',0,1),(38,'Cedis','GHC','¢',0,0),(39,'Pounds','GIP','£',0,1),(40,'Quetzales','GTQ','Q',0,1),(41,'Pounds','GGP','£',0,0),(42,'Dollars','GYD','$',0,1),(43,'Lempiras','HNL','L',0,1),(44,'Dollars','HKD','$',1,1),(45,'Forint','HUF','Ft',1,1),(46,'Kronur','ISK','kr',0,1),(47,'Rupees','INR','Rp',1,1),(48,'Rupiahs','IDR','Rp',0,1),(49,'Rials','IRR','﷼',0,0),(50,'Pounds','IMP','£',0,0),(51,'New Shekels','ILS','₪',1,1),(52,'Dollars','JMD','J$',0,1),(53,'Yen','JPY','¥',1,1),(54,'Pounds','JEP','£',0,0),(55,'Tenge','KZT','лв',0,1),(56,'Won','KPW','₩',0,0),(57,'Won','KRW','₩',0,1),(58,'Soms','KGS','лв',0,1),(59,'Kips','LAK','₭',0,1),(60,'Lati','LVL','Ls',0,0),(61,'Pounds','LBP','£',0,1),(62,'Dollars','LRD','$',0,1),(63,'Switzerland Francs','CHF','CHF',1,1),(64,'Litai','LTL','Lt',0,0),(65,'Denars','MKD','ден',0,1),(66,'Ringgits','MYR','RM',1,1),(67,'Rupees','MUR','₨',0,1),(68,'Pesos','MXN','$',1,1),(69,'Tugriks','MNT','₮',0,1),(70,'Meticais','MZN','MT',0,1),(71,'Dollars','NAD','$',0,1),(72,'Rupees','NPR','₨',0,1),(73,'Guilders','ANG','ƒ',0,1),(74,'Dollars','NZD','$',1,1),(75,'Cordobas','NIO','C$',0,1),(76,'Nairas','NGN','₦',0,1),(77,'Krone','NOK','kr',1,1),(78,'Rials','OMR','﷼',0,0),(79,'Rupees','PKR','₨',0,1),(80,'Balboa','PAB','B/.',0,1),(81,'Guarani','PYG','Gs',0,1),(82,'Nuevos Soles','PEN','S/.',0,1),(83,'Pesos','PHP','Php',1,1),(84,'Zlotych','PLN','zł',1,1),(85,'Rials','QAR','﷼',0,1),(86,'New Lei','RON','lei',0,1),(87,'Rubles','RUB','руб',1,1),(88,'Pounds','SHP','£',0,1),(89,'Riyals','SAR','﷼',0,1),(90,'Dinars','RSD','Дин.',0,1),(91,'Rupees','SCR','₨',0,1),(92,'Dollars','SGD','$',1,1),(93,'Dollars','SBD','$',0,1),(94,'Shillings','SOS','S',0,1),(95,'Rand','ZAR','R',0,1),(96,'Rupees','LKR','₨',0,1),(97,'Kronor','SEK','kr',1,1),(98,'Dollars','SRD','$',0,1),(99,'Pounds','SYP','£',0,0),(100,'New Dollars','TWD','NT$',1,1),(101,'Baht','THB','฿',1,1),(102,'Dollars','TTD','TT$',0,1),(103,'Lira','TRY','TL',0,1),(104,'Liras','TRL','£',0,0),(105,'Dollars','TVD','$',0,0),(106,'Hryvnia','UAH','₴',0,1),(107,'Pesos','UYU','$U',0,1),(108,'Sums','UZS','лв',0,1),(109,'Bolivares Fuertes','VEF','Bs',0,0),(110,'Dong','VND','₫',0,1),(111,'Rials','YER','﷼',0,1),(112,'Zimbabwe Dollars','ZWD','Z$',0,0);
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enrol`
--

DROP TABLE IF EXISTS `enrol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enrol` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `course_id` int DEFAULT NULL,
  `date_added` int DEFAULT NULL,
  `last_modified` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enrol`
--

LOCK TABLES `enrol` WRITE;
/*!40000 ALTER TABLE `enrol` DISABLE KEYS */;
/*!40000 ALTER TABLE `enrol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frontend_settings`
--

DROP TABLE IF EXISTS `frontend_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `frontend_settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frontend_settings`
--

LOCK TABLES `frontend_settings` WRITE;
/*!40000 ALTER TABLE `frontend_settings` DISABLE KEYS */;
INSERT INTO `frontend_settings` VALUES (1,'banner_title','Learn on your schedule'),(2,'banner_sub_title','Study any topic, anytime. Explore thousands of courses for the lowest price ever!'),(4,'about_us','<p></p><h2><span xss=removed>This is about us</span></h2><p><span xss=\"removed\">Welcome to Academy. It will help you to learn in a new ways</span></p>'),(10,'terms_and_condition','<h2>Terms and Condition</h2>This is the Terms and condition page for your companys'),(11,'privacy_policy','<p></p><p></p><h2><span xss=\"removed\">Privacy Policy</span><br></h2>This is the Privacy Policy page for your companys<p></p><p><b>This is another</b> <u><a href=\"https://youtube.com/watch?v=PHgc8Q6qTjc\" target=\"_blank\">thing you will</a></u> <span xss=\"removed\">not understand</span>.</p>'),(13,'theme','default'),(14,'cookie_note','This website uses cookies to personalize content and analyse traffic in order to offer you a better experience.'),(15,'cookie_status','active'),(16,'cookie_policy','<h1>Cookie policy</h1><ol><li>Cookies are small text files that can be used by websites to make a user\'s experience more efficient.</li><li>The law states that we can store cookies on your device if they are strictly necessary for the operation of this site. For all other types of cookies we need your permission.</li><li>This site uses different types of cookies. Some cookies are placed by third party services that appear on our pages.</li></ol>'),(17,'banner_image','home-banner.jpg'),(18,'light_logo','logo-light.png'),(19,'dark_logo','logo-dark.png'),(20,'small_logo','logo-light-sm.png'),(21,'favicon','favicon.png'),(22,'recaptcha_status','0'),(23,'recaptcha_secretkey','recaptcha-secretkey'),(24,'recaptcha_sitekey','recaptcha-sitekey'),(25,'refund_policy','<h2><span xss=\"removed\">Refund Policy</span></h2>'),(26,'feedback_status','active');
/*!40000 ALTER TABLE `frontend_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `language` (
  `phrase_id` int NOT NULL AUTO_INCREMENT,
  `phrase` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `english` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`phrase_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lesson`
--

DROP TABLE IF EXISTS `lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lesson` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int DEFAULT NULL,
  `section_id` int DEFAULT NULL,
  `video_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int DEFAULT NULL,
  `last_modified` int DEFAULT NULL,
  `lesson_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `attachment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `is_free` int NOT NULL DEFAULT '0',
  `order` int NOT NULL DEFAULT '0',
  `video_type_for_mobile_application` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url_for_mobile_application` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration_for_mobile_application` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lesson`
--

LOCK TABLES `lesson` WRITE;
/*!40000 ALTER TABLE `lesson` DISABLE KEYS */;
/*!40000 ALTER TABLE `lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `log` (
  `id` int NOT NULL,
  `from` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `message` (
  `message_id` int NOT NULL AUTO_INCREMENT,
  `message_thread_code` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `message` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `sender` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `timestamp` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `read_status` int DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,'0b45370e9e5bf80','Testing ','4','1632966122',NULL);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_thread`
--

DROP TABLE IF EXISTS `message_thread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `message_thread` (
  `message_thread_id` int NOT NULL AUTO_INCREMENT,
  `message_thread_code` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `sender` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `receiver` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `last_message_timestamp` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`message_thread_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_thread`
--

LOCK TABLES `message_thread` WRITE;
/*!40000 ALTER TABLE `message_thread` DISABLE KEYS */;
INSERT INTO `message_thread` VALUES (1,'0b45370e9e5bf80','4','2',NULL);
/*!40000 ALTER TABLE `message_thread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `payment_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `date_added` int DEFAULT NULL,
  `last_modified` int DEFAULT NULL,
  `admin_revenue` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `instructor_revenue` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `instructor_payment_status` int DEFAULT '0',
  `transaction_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `session_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `coupon` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payout`
--

DROP TABLE IF EXISTS `payout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payout` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `date_added` int DEFAULT NULL,
  `last_modified` int DEFAULT NULL,
  `status` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payout`
--

LOCK TABLES `payout` WRITE;
/*!40000 ALTER TABLE `payout` DISABLE KEYS */;
/*!40000 ALTER TABLE `payout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int DEFAULT NULL,
  `permissions` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,2,'[\"category\",\"course\",\"user\",\"instructor\",\"student\",\"enrolment\",\"messaging\",\"addon\",\"theme\",\"settings\",\"coupon\",\"revenue\"]'),(2,5,'[]'),(3,6,'[]');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `quiz_id` int DEFAULT NULL,
  `title` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `number_of_options` int DEFAULT NULL,
  `options` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `correct_answers` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `order` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rating` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `rating` double DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `ratable_id` int DEFAULT NULL,
  `ratable_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int DEFAULT NULL,
  `last_modified` int DEFAULT NULL,
  `review` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` VALUES (1,NULL,NULL,NULL,'course',1632960000,NULL,NULL);
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int DEFAULT NULL,
  `last_modified` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin',1234567890,1234567890),(2,'User',1234567890,1234567890);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `section` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int DEFAULT NULL,
  `order` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section`
--

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'language','english'),(2,'system_name','QMV Learning System'),(3,'system_title','Academy Learning Club'),(4,'system_email','academy@example.com'),(5,'address','Sydeny, Australia'),(6,'phone','+143-52-9933631'),(7,'purchase_code','f2c67404-dc1a-4bda-b2b9-622e2e40b481'),(8,'paypal','[{\"active\":\"1\",\"mode\":\"sandbox\",\"sandbox_client_id\":\"AfGaziKslex-scLAyYdDYXNFaz2aL5qGau-SbDgE_D2E80D3AFauLagP8e0kCq9au7W4IasmFbirUUYc\",\"sandbox_secret_key\":\"EMa5pCTuOpmHkhHaCGibGhVUcKg0yt5-C3CzJw-OWJCzaXXzTlyD17SICob_BkfM_0Nlk7TWnN42cbGz\",\"production_client_id\":\"1234\",\"production_secret_key\":\"12345\"}]'),(9,'stripe_keys','[{\"active\":\"0\",\"testmode\":\"on\",\"public_key\":\"pk_test_CAC3cB1mhgkJqXtypYBTGb4f\",\"secret_key\":\"sk_test_iatnshcHhQVRXdygXw3L2Pp2\",\"public_live_key\":\"pk_live_xxxxxxxxxxxxxxxxxxxxxxxx\",\"secret_live_key\":\"sk_live_xxxxxxxxxxxxxxxxxxxxxxxx\"}]'),(10,'youtube_api_key','youtube-api-key'),(11,'vimeo_api_key','vimeo-api-key'),(12,'slogan','A course based video CMS'),(13,'text_align',NULL),(14,'allow_instructor','1'),(15,'instructor_revenue','70'),(16,'system_currency','VND'),(17,'paypal_currency','USD'),(18,'stripe_currency','USD'),(19,'author','Creativeitem'),(20,'currency_position','right-space'),(21,'website_description','Nice application'),(22,'website_keywords','LMS,Learning Management System,Creativeitem,demo,hello,How are you'),(23,'footer_text',''),(24,'footer_link','http://creativeitem.com/'),(25,'protocol','smtp'),(26,'smtp_host','ssl://smtp.gmail.com'),(27,'smtp_port','465'),(28,'smtp_user','Enter your email address'),(29,'smtp_pass','Enter your email password'),(30,'version','5.3'),(31,'student_email_verification','disable'),(32,'instructor_application_note','Fill all the fields carefully and share if you want to share any document with us it will help us to evaluate you as an instructor.'),(33,'razorpay_keys','[{\"active\":\"0\",\"key\":\"rzp_test_J60bqBOi1z1aF5\",\"secret_key\":\"uk935K7p4j96UCJgHK8kAU4q\",\"theme_color\":\"#c7a600\"}]'),(34,'razorpay_currency','USD'),(35,'fb_app_id','facebook-app-id'),(36,'fb_app_secret','facebook-app-secret-key'),(37,'fb_social_login','0'),(38, 'exchange_rate', '1'),(39, 'discord_social_login', '0'),(40, 'discord_client_id', 'discord_client_id'),(41, 'discord_client_secret', 'discord_client_secret'),(42, 'guild_ids_are_allowed_access_to_website', '');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tag` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tagable_id` int DEFAULT NULL,
  `tagable_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int DEFAULT NULL,
  `last_modified` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_links` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `biography` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `role_id` int DEFAULT NULL,
  `date_added` int DEFAULT NULL,
  `last_modified` int DEFAULT NULL,
  `watch_history` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `wishlist` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `title` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `paypal_keys` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `stripe_keys` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `verification_code` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` int DEFAULT NULL,
  `is_instructor` int DEFAULT '0',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Tran Vu ','Viet Anh','anhtvv@bachasoftware.com','3671aeaa9b3fac05f87704b8e238ec60f08e572c','{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,NULL),(2,'Quyet','Dao Van','quyetdv@bachasoftware.com','3671aeaa9b3fac05f87704b8e238ec60f08e572c','{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}','',1,1632964441,1632970839,'[]','[]','','[{\"production_client_id\":null,\"production_secret_key\":null}]','[{\"public_live_key\":null,\"secret_live_key\":null}]',NULL,1,1,'c776ddf3bea02690d361a14713c0eadb'),(4,'Việt','Anh','anhtvv.bhsoft@gmail.com','273d28a634257ce2912a42f27539c3b9eb47b74e','{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}','<p>IT Expert</p>',2,1632966048,NULL,'[]','[]',NULL,'[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]','[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]',NULL,1,1,'de78e6a6863ca3296f5bbd587b2d0652'),(5,'Quach','Hao','quachhao@gmail.com','28559ac23d6146b24cee992f846541eb1a8194bb','{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}','',1,1632970384,NULL,'[]','[]',NULL,'[{\"production_client_id\":null,\"production_secret_key\":null}]','[{\"public_live_key\":null,\"secret_live_key\":null}]',NULL,1,1,'89c64dc3df736ab775a86a8ed91745d8'),(6,'Nam','Mai','mainam.90@gmail.com','81741ec32d7d70aeb8ad7c9dde9b9e35f22e1c53','{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}','',1,1632970548,NULL,'[]','[]',NULL,'[{\"production_client_id\":null,\"production_secret_key\":null}]','[{\"public_live_key\":null,\"secret_live_key\":null}]',NULL,1,1,'f98185bfdb5678d34087bac4f5dbe732');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `watch_histories`
--

DROP TABLE IF EXISTS `watch_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `watch_histories` (
  `watch_history_id` int NOT NULL AUTO_INCREMENT,
  `course_id` int NOT NULL,
  `student_id` int NOT NULL,
  `watching_lesson_id` int NOT NULL,
  `date_added` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_updated` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`watch_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `watch_histories`
--

LOCK TABLES `watch_histories` WRITE;
/*!40000 ALTER TABLE `watch_histories` DISABLE KEYS */;
/*!40000 ALTER TABLE `watch_histories` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-30  3:30:19
