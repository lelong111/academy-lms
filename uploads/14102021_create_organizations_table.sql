CREATE TABLE `organizations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `abbreviation` VARCHAR(255) NOT NULL,
  `active` INT NULL DEFAULT 0,
  `date_added` INT NULL,
  `last_modified` INT NULL,
  `free_for_org_member` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`));

INSERT INTO `organizations` (`name`, `abbreviation`, `active`, `date_added`, `last_modified`, `free_for_org_member`) VALUES ('QMV', 'qmv', '1', '1634184000', '1634184000', '1');
-- Add organizations permissions for first admin
UPDATE `permissions` SET `permissions` = '[\"category\",\"course\",\"user\",\"instructor\",\"student\",\"enrolment\",\"messaging\",\"addon\",\"theme\",\"settings\",\"revenue\",\"admin\",\"admins\",\"coupon\",\"organizations\"]' WHERE (`id` = '1');

ALTER TABLE `users` 
ADD COLUMN `organization_id` INT NULL;
