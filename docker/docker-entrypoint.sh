#!/bin/bash
EXPORT_COMMAND='mysql -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE" < /docker-entrypoint-initdb.d/$MYSQL_DATABASE.sql'
docker-compose exec mysql sh -c "$EXPORT_COMMAND"
