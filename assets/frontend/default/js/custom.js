function checkNotEmpty() {
    let message = $('form.send-message textarea[name="message"]');
    let submitButton = $('form.send-message button[type="submit"]');
    if (message.val() == '') {
        submitButton.attr('disabled', 'disabled');
    } else {
        submitButton.removeAttr('disabled');
    }
}

function checkRequiredFields() {
    var pass = 1;
    $('form.required-form').find('input, select, textarea').each(function() {
        if ($(this).prop('required')) {
            if ($(this).val() === "") {
                pass = 0;
            }
        }
    });
    return pass;
}

function updateUserProfile() {
    if (checkRequiredFields()) {
        $('form.required-form').submit();
    } else {
        error_required_field();
    }
}