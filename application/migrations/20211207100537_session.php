<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Session extends CI_Migration
{

        public function up()
        {
                $this->dbforge->add_field(array(

                        'id' => array(
                                'type' => 'int',
                                'constraint' => 11,
                                'unsigned' => true,
                                'auto_increment' => true
                        ),

                        'user_id' => array(
                                'type' => 'int',
                                'constraint' => 11,
                                'unsigned' => true,
                        ),

                        'session_id' => array(
                                'type' => 'varchar',
                                'constraint' => 100,
                        ),

                        'updated_at' => array(
                                'type' => 'datetime'
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('sessions');
        }

        public function down()
        {
        }
}
