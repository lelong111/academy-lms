<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Discord_login extends CI_Migration
{

        public function up()
        {
                $this->db->insert('settings', [
                        'key' => 'discord_social_login',
                        'value' => '0'
                ]);
                $this->db->insert('settings', [
                        'key' => 'discord_client_id',
                        'value' => 'client_id'
                ]);
                $this->db->insert('settings', [
                        'key' => 'discord_client_secret',
                        'value' => 'client_secret'
                ]);
                $this->db->insert('settings', [
                        'key' => 'guild_ids_are_allowed_access_to_website',
                        'value' => ''
                ]);
        }

        public function down()
        {
        }
}
