<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Buy_Course extends CI_Migration
{

        public function up()
        {
                $this->db->insert('frontend_settings', [
                        'key' => 'buy_course_info',
                        'value' => ''
                ]);
                $this->db->insert('frontend_settings', [
                        'key' => 'buy_course_status',
                        'value' => 'active'
                ]);
                $this->db->insert('frontend_settings', [
                        'key' => 'cart_status',
                        'value' => 'active'
                ]);
        }

        public function down()
        {
        }
}