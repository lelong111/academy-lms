<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Google_Analytics extends CI_Migration
{

        public function up()
        {
                $this->db->insert('frontend_settings', [
                        'key' => 'google_analytics_code',
                        'value' => ''
                ]);
        }

        public function down()
        {
        }
}