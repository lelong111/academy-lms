<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Feedback_status extends CI_Migration
{

        public function up()
        {
                $this->db->insert('frontend_settings', [
                        'key' => 'feedback_status',
                        'value' => 'active'
                ]);
        }

        public function down()
        {
        }
}
