<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Discord_Role extends CI_Migration
{

        public function up()
        {
                $this->db->insert('settings', [
                        'key' => 'role_id_allowed_access_video',
                        'value' => ''
                ]);
                $this->db->insert('settings', [
                        'key' => 'organization_id_allow',
                        'value' => ''
                ]);
                $this->db->insert('settings', [
                        'key' => 'qmv_login_api',
                        'value' => ''
                ]);
                $this->db->insert('settings', [
                        'key' => 'qmv_auth_code',
                        'value' => ''
                ]);
        }

        public function down()
        {
        }
}