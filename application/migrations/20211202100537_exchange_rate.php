<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Exchange_rate extends CI_Migration
{

        public function up()
        {
                $this->db->insert('settings', [
                        'key' => 'exchange_rate',
                        'value' => '1'
                ]);
        }

        public function down()
        {
        }
}
