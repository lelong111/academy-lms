<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->library('session');
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    public function index()
    {
        if ($this->session->userdata('admin_login')) {
            redirect(site_url('admin'), 'refresh');
        } elseif ($this->session->userdata('user_login')) {
            redirect(site_url('user'), 'refresh');
        } else {
            redirect(site_url('home/login'), 'refresh');
        }
    }

    public function discord_login()
    {
        $accessToken = $this->session->__get('discord_access_token');
        $this->social_login_modal->discord_login($accessToken);
    }

    public function validate_login($from = "")
    {
        if ($from != 'session') {
            if ($this->crud_model->check_recaptcha() == false && get_frontend_settings('recaptcha_status') == true) {
                $this->session->set_flashdata('error_message', get_phrase('recaptcha_verification_failed'));
                redirect(site_url('home/login'), 'refresh');
            }
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $credential = array('email' => $email, 'password' => sha1($password), 'status' => 1);
        } else {
            $email = $this->session->__get('session_login')['email'];
            $password = $this->session->__get('session_login')['password'];
            $this->session->__set('session_login', null);
            $this->session->__set('session_alert', null);
            $credential = array('email' => $email, 'password' => $password, 'status' => 1);
        }

        // Checking login credential for admin
        $query = $this->db->get_where('users', $credential);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($from == 'session') {
                $this->crud_model->delete_oldest_session($row->id);
            }
            $session = $this->crud_model->add_sessions($row->id, $this->session->__get('session_id'));
            if ($session) {
                $this->session->set_userdata('user_id', $row->id);
                $this->session->set_userdata('role_id', $row->role_id);
                $this->session->set_userdata('role', get_user_role('user_role', $row->id));
                $this->session->set_userdata('name', $row->first_name . ' ' . $row->last_name);
                $this->session->set_userdata('is_instructor', $row->is_instructor);
                $this->session->set_flashdata('flash_message', get_phrase('welcome') . ' ' . $row->first_name . ' ' . $row->last_name);
                if ($row->role_id == 1) {
                    $this->session->set_userdata('admin_login', '1');
                    redirect(site_url('admin/dashboard'), 'refresh');
                } else if ($row->role_id == 2) {
                    $this->session->set_userdata('user_login', '1');

                    if ($this->session->userdata('url_history')) {
                        redirect($this->session->userdata('url_history'), 'refresh');
                    }
                    redirect(site_url('home'), 'refresh');
                }
            } else {
                $this->session->__set('session_login', [
                    'email' => $email,
                    'password' => sha1($password)
                ]);
                $this->session->__set('session_alert', true);
                redirect(site_url('home/login'), 'refresh');
            }
        } else {
            $this->session->set_flashdata('error_message', get_phrase('invalid_login_credentials'));
            redirect(site_url('home/login'), 'refresh');
        }
    }

    public function fb_validate_login($access_token = "", $fb_user_id = "")
    {
        $this->social_login_modal->fb_validate_login($access_token, $fb_user_id);
    }

    public function discord_validate_login()
    {
        $code = $this->input->get('code');
        $url = "https://discord.com/api/oauth2/token";
        $redirectUri = site_url('login/discord_validate_login');
        $data = array(
            "client_id" => get_settings('discord_client_id'),
            "client_secret" => get_settings('discord_client_secret'),
            "grant_type" => "authorization_code",
            "code" => $code,
            "redirect_uri" => $redirectUri
        );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        $results = json_decode($response, true);
        $this->session->__set('discord_access_token', $results['access_token']);
        $this->load->view('frontend/default/discord_login_success.php');
    }

    public function register()
    {

        if ($this->crud_model->check_recaptcha() == false && get_frontend_settings('recaptcha_status') == true) {
            $this->session->set_flashdata('error_message', get_phrase('recaptcha_verification_failed'));
            redirect(site_url('home/login'), 'refresh');
        }

        $data['first_name'] = html_escape($this->input->post('first_name'));
        $data['last_name']  = html_escape($this->input->post('last_name'));
        $data['email']  = html_escape($this->input->post('email'));
        $data['password']  = sha1($this->input->post('password'));
        $confirm_password = sha1($this->input->post('confirm_password'));

        if (empty($data['first_name']) || empty($data['last_name']) || empty($data['email']) || empty($data['password']) || empty($confirm_password)) {
            $this->session->set_flashdata('error_message', site_phrase('your_sign_up_form_is_empty') . '. ' . site_phrase('fill_out_the_form with_your_valid_data'));
            redirect(site_url('home/sign_up'), 'refresh');
        }

        $regexPassword = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,16}$/';
        if (!preg_match($regexPassword, $this->input->post('password'))) {
            $this->session->set_flashdata('error_message', site_phrase('error_invalid_password'));
            redirect(site_url('home/sign_up'), 'refresh');
        }

        if ($data['password'] != $confirm_password) {
            $this->session->set_flashdata('error_message', site_phrase('error_password_mismatch'));
            redirect(site_url('home/sign_up'), 'refresh');
        }

        $verification_code =  rand(100000, 200000);
        $data['verification_code'] = $verification_code;

        if (get_settings('student_email_verification') == 'enable') {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }

        $data['wishlist'] = json_encode(array());
        $data['watch_history'] = json_encode(array());
        $data['date_added'] = strtotime(date("Y-m-d H:i:s"));
        $social_links = array(
            'facebook' => "",
            'twitter'  => "",
            'linkedin' => ""
        );
        $data['social_links'] = json_encode($social_links);
        $data['role_id']  = 2;

        // Add paypal keys
        $paypal_info = array();
        $paypal['production_client_id'] = "";
        array_push($paypal_info, $paypal);
        $data['paypal_keys'] = json_encode($paypal_info);
        // Add Stripe keys
        $stripe_info = array();
        $stripe_keys = array(
            'public_live_key' => "",
            'secret_live_key' => ""
        );
        array_push($stripe_info, $stripe_keys);
        $data['stripe_keys'] = json_encode($stripe_info);

        $validity = $this->user_model->check_duplication('on_create', $data['email']);

        if ($validity === 'unverified_user' || $validity == true) {
            if ($validity === true) {
                $this->user_model->register_user($data);
            } else {
                $this->user_model->register_user_update_code($data);
            }

            if (get_settings('student_email_verification') == 'enable') {
                $this->email_model->send_email_verification_mail($data['email'], $verification_code);

                if ($validity === 'unverified_user') {
                    $this->session->set_flashdata('info_message', get_phrase('you_have_already_registered') . '. ' . get_phrase('please_verify_your_email_address'));
                } else {
                    $this->session->set_flashdata('flash_message', get_phrase('your_registration_has_been_successfully_done') . '. ' . get_phrase('please_check_your_mail_inbox_to_verify_your_email_address') . '.');
                }
                $this->session->set_userdata('register_email', $this->input->post('email'));
                redirect(site_url('home/verification_code'), 'refresh');
            } else {
                $this->session->set_flashdata('flash_message', get_phrase('your_registration_has_been_successfully_done'));
                redirect(site_url('home/login'), 'refresh');
            }
        } else {
            $this->session->set_flashdata('error_message', get_phrase('you_have_already_registered'));
            redirect(site_url('home/sign_up'), 'refresh');
        }
    }

    public function checkInvalidSession()
    {
        header('Content-Type: application/json');
        $user_id = $this->session->userdata('user_id');
        if (!empty($user_id)) {
            echo $this->crud_model->checkInvalidSession($user_id, $this->session->__get('session_id')) ? 1 : 0;
        } else {
            echo 1;
        }
    }

    public function logout($from = "")
    {
        //destroy sessions of specific userdata. We've done this for not removing the cart session
        $this->session_destroy();
        if ($from == 'session') {
            $this->session->set_flashdata('error_message', get_phrase('your_session_has_expired'));
        }
        redirect(site_url('home'), 'refresh');
    }

    public function session_destroy()
    {
        $this->crud_model->delete_session($this->session->userdata('user_id'), $this->session->__get('session_id'));
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('role_id');
        $this->session->unset_userdata('role');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('is_instructor');
        $this->session->unset_userdata('url_history');
        if ($this->session->userdata('admin_login') == 1) {
            $this->session->unset_userdata('admin_login');
        } else {
            $this->session->unset_userdata('user_login');
        }
        if($this->session->userdata('user_type')){
            $this->session->unset_userdata('user_type');
        }

        if ($this->session->userdata('fb_login') == 1) {
            $this->session->unset_userdata('fb_login');
        }
    }

    function forgot_password($from = "")
    {
        if ($this->crud_model->check_recaptcha() == false && get_frontend_settings('recaptcha_status') == true) {
            $this->session->set_flashdata('error_message', get_phrase('recaptcha_verification_failed'));
            redirect(site_url('home/login'), 'refresh');
        }
        $email = $this->input->post('email');
        //resetting user password here
        $new_password = substr(md5(rand(100000000, 20000000000)), 0, 7);

        // Checking credential for admin
        $query = $this->db->get_where('users', array('email' => $email));
        if ($query->num_rows() > 0) {
            $this->db->where('email', $email);
            if ($from == 'frontend') {
                $this->db->update('users', array('reset_password_token' => $new_password));
                // send reset token password to user email
                $this->email_model->reset_email_password_token($new_password, $email);
                $this->session->set_flashdata('flash_message', get_phrase('please_check_your_email'));
            } else {
                $this->db->update('users', array('password' => sha1($new_password)));
                // send new password to user email
                $this->email_model->password_reset_email($new_password, $email);
                $this->session->set_flashdata('flash_message', get_phrase('please_check_your_email_for_new_password'));
            }
            if ($from == 'backend') {
                redirect(site_url('login'), 'refresh');
            } else {
                redirect(site_url('home'), 'refresh');
            }
        } else {
            $this->session->set_flashdata('error_message', get_phrase('password_reset_failed'));
            if ($from == 'backend') {
                redirect(site_url('login'), 'refresh');
            } else {
                redirect(site_url('home'), 'refresh');
            }
        }
    }

    public function resend_verification_code()
    {
        $email = $this->input->post('email');
        $verification_code = $this->db->get_where('users', array('email' => $email))->row('verification_code');
        $this->email_model->send_email_verification_mail($email, $verification_code);

        return true;
    }

    public function verify_email_address()
    {
        $email = $this->input->post('email');
        $verification_code = $this->input->post('verification_code');
        $user_details = $this->db->get_where('users', array('email' => $email, 'verification_code' => $verification_code));
        if ($user_details->num_rows() > 0) {
            $user_details = $user_details->row_array();
            $updater = array(
                'status' => 1
            );
            $this->db->where('id', $user_details['id']);
            $this->db->update('users', $updater);
            $this->session->set_flashdata('flash_message', get_phrase('congratulations') . '!' . get_phrase('your_email_address_has_been_successfully_verified') . '.');
            $this->session->set_userdata('register_email', null);
            echo true;
        } else {
            $this->session->set_flashdata('error_message', get_phrase('the_verification_code_is_wrong') . '.');
            echo false;
        }
    }


    function check_recaptcha_with_ajax()
    {
        if ($this->crud_model->check_recaptcha()) {
            echo true;
        } else {
            echo false;
        }
    }

    public function renew_password($token = "")
    {
        $query = $this->db->get_where('users', array('reset_password_token' => $token));
        if ($token && $query->num_rows() > 0) {
            $page_data['token'] = $token;
            $page_data['user'] = $query->row_array();
            $page_data['page_name'] = 'renew_password';
            $page_data['page_title'] = site_phrase('renew_password');
            $this->load->view('frontend/' . get_frontend_settings('theme') . '/index', $page_data);
        } else {
            $this->session->set_flashdata('error_message', get_phrase('password_reset_failed'));
            redirect(site_url('home'), 'refresh');
        }
    }

    public function update_password()
    {
        $query = $this->db->get_where('users', array('reset_password_token' => $this->input->post('token'), 'email' => $this->input->post('email')));
        if (!empty($this->input->post('email')) && $query->num_rows() > 0) {
            $this->db->where('email', $this->input->post('email'));
            $data = [
                'reset_password_token' => null,
                'password' => sha1($this->input->post('password')),
                'last_modified' => strtotime(date("Y-m-d H:i:s"))
            ];

            if ($discord_id = $this->input->post('discord_id')) {
                $user = $query->row_array();
                $social_link = json_decode($user['social_links'], true);
                $social_link['discord_id'] = html_escape($discord_id);
                $data['social_links'] = json_encode($social_link);
            }

            $this->db->update('users', $data);
            $this->session->set_flashdata('flash_message', get_phrase('your_password_has_been_changed'));
        } else {
            $this->session->set_flashdata('error_message', get_phrase('password_reset_failed'));
        }
        redirect(site_url('home'), 'refresh');
    }

    public function ajax_check_duplicate_email()
    {
        $email = $this->input->post('email');
        $method = $this->input->post('method');
        $user_id = $this->session->userdata('user_id');
        $check = $this->user_model->check_duplication($method, $email, $user_id);
        echo json_encode($check);
    }

    public function ajax_check_email_exist()
    {
        $email = $this->input->post('email');
        $check = $this->db->get_where('users', array('email' => $email));
        if ($check->num_rows() > 0) {
            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
    }

    public function ajax_login($from = ''){
        if ($from != 'session') {
            if ($this->crud_model->check_recaptcha() == false && get_frontend_settings('recaptcha_status') == true) {
                echo json_encode([
                    'status'=>'error',
                    'message'=>get_phrase('recaptcha_verification_failed')
                ]);
                return;
            }
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $credential = array('email' => $email, 'password' => sha1($password), 'status' => 1);
        } else {
            $email = $this->session->__get('session_login')['email'];
            $password = $this->session->__get('session_login')['password'];
            $this->session->__set('session_login', null);
            $this->session->__set('session_alert', null);
            $credential = array('email' => $email, 'password' => $password, 'status' => 1);
        }

        // Checking login credential for admin
        $query = $this->db->get_where('users', $credential);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($from == 'session') {
                $this->crud_model->delete_oldest_session($row->id);
            }
            $session = $this->crud_model->add_sessions($row->id, $this->session->__get('session_id'));
            if ($session) {
                $this->session->set_userdata('user_id', $row->id);
                $this->session->set_userdata('role_id', $row->role_id);
                $this->session->set_userdata('role', get_user_role('user_role', $row->id));
                $this->session->set_userdata('name', $row->first_name . ' ' . $row->last_name);
                $this->session->set_userdata('is_instructor', $row->is_instructor);
                $this->session->set_flashdata('flash_message', get_phrase('welcome') . ' ' . $row->first_name . ' ' . $row->last_name);
                if ($row->role_id == 1) {
                    $this->session->set_userdata('admin_login', '1');
                    echo json_encode([
                        'status'=>'success',
                        'is_admin'=> true
                    ]);
                } else if ($row->role_id == 2) {
                    $this->session->set_userdata('user_login', '1');
                    echo json_encode([
                        'status'=>'success'
                    ]);
                }
                return;
            } else {
                $this->session->__set('session_login', [
                    'email' => $email,
                    'password' => sha1($password)
                ]);
                $this->session->__set('session_alert', true);
                echo json_encode([
                    'status'=>'success'
                ]);
                return;
            }
        } else {
            echo json_encode([
                'status'=>'error',
                'message'=>get_phrase('invalid_login_credentials')
            ]);
            return;
        }
    }

    public function qmv_ajax_login(){
        if ($this->crud_model->check_recaptcha() == false && get_frontend_settings('recaptcha_status') == true) {
            echo json_encode([
                'status'=>'error',
                'message'=>get_phrase('recaptcha_verification_failed')
            ]);
            return;
        }
        
        $response = $this->user_model->qmv_login();

        if($response['status'] == 'error'){
            echo json_encode([
                'status'=> 'error',
                'message'=> $response['message']
            ]);
            return;
        }

        if($response['status'] == 'success'){
            $user = $response['data'];
    
            $this->session->set_userdata('user_id', $user['id']);
            $this->session->set_userdata('role_id', $user['role_id']);
            $this->session->set_userdata('role', get_user_role('user_role', $user['id']));
            $this->session->set_userdata('name', $user['first_name'] . ' ' . $user['last_name']);
            $this->session->set_userdata('is_instructor', $user['is_instructor']);
            $this->session->set_flashdata('flash_message', get_phrase('welcome') . ' ' . $user['first_name'] . ' ' . $user['last_name']);
            $this->session->set_userdata('user_login', '1');
            $this->session->set_userdata('user_type', 'qmv');
    
            echo json_encode([
                'status'=>'success'
            ]);
            die;
        }
    }


}
