<section class="category-header-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home'); ?>"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item">
                            <a href="#">
                                <?php echo $page_title; ?>
                            </a>
                        </li>
                    </ol>
                </nav>
                <h1 class="category-name">
                    <?php echo site_phrase('renew_password'); ?>
                </h1>
            </div>
        </div>
    </div>
</section>
<?php echo $this->session->userdata('is_instructor'); ?>
<section class="category-course-list-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9">
              <div class="user-dashboard-box mt-3">
                  <div class="user-dashboard-content w-100 register-form">
                      <div class="content-title-box">
                          <div class="title"><?php echo site_phrase('renew_password_form'); ?></div>
                      </div>
                      <form action="<?php echo site_url('login/update_password'); ?>" method="post" id="update_password">
                          <div class="content-box">
                              <div class="basic-group">
                                  <?php if(!empty($user["organization_id"]) && $user["organization_id"] == 1):?>
                                  <div class="form-group">
                                      <label for="discord_id"><span class="input-field-icon"><i class="fas fa-user"></i></span> <?php echo site_phrase('discord_id'); ?>:</label>
                                      <input type="text" class="form-control" name = "discord_id" id="discord_id" placeholder="<?php echo site_phrase('discord_id'); ?>" value="" required>
                                  </div>
                                  <?php endif?>
                                  <div class="form-group">
                                    <input type="hidden" name = "token" id="token" value="<?php echo $token ?>" required>
                                      <label for="registration-email"><span class="input-field-icon"><i class="fas fa-envelope"></i></span> <?php echo site_phrase('email'); ?>:</label>
                                      <input type="email" class="form-control" name = "email" id="registration-email" placeholder="<?php echo site_phrase('email'); ?>" value="<?php echo $user['email'] ?>" readonly required>
                                  </div>
                                  <div class="form-group">
                                      <label for="registration-password"><span class="input-field-icon"><i class="fas fa-lock"></i></span> <?php echo site_phrase('password'); ?>:</label>
                                      <input type="password" class="form-control" name = "password" id="registration-password" placeholder="<?php echo site_phrase('password'); ?>" value="" required>
                                  </div>
                              </div>
                          </div>
                          <div class="content-update-box">
                                <button class="btn" type="submit"><?php echo site_phrase('submit'); ?></button>
                          </div>
                      </form>
                  </div>
              </div>
            </div>
        </div>
    </div>
</section>
