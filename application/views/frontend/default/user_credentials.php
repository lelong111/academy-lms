<section class="page-header-area my-course-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><?php echo site_phrase('purchase_history'); ?></h1>
                <ul>
                    <li><a href="<?php echo site_url('home/my_courses'); ?>"><?php echo site_phrase('all_courses'); ?></a></li>
                    <li><a href="<?php echo site_url('home/my_wishlist'); ?>"><?php echo site_phrase('wishlists'); ?></a></li>
                    <li><a href="<?php echo site_url('home/my_messages'); ?>"><?php echo site_phrase('my_messages'); ?></a></li>
                    <li><a href="<?php echo site_url('home/purchase_history'); ?>"><?php echo site_phrase('purchase_history'); ?></a></li>
                    <li class="active"><a href=""><?php echo site_phrase('user_profile'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="user-dashboard-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="user-dashboard-box">
                    <div class="user-dashboard-sidebar">
                        <div class="user-box">
                            <img src="<?php echo $this->user_model->get_user_image_url($this->session->userdata('user_id')); ?>" alt="" class="img-fluid">
                            <div class="name"><?php echo $user_details['first_name'].' '.$user_details['last_name']; ?></div>
                        </div>
                        <div class="user-dashboard-menu">
                            <ul>
                                <li><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo site_phrase('profile'); ?></a></li>
                                <li class="active"><a href="<?php echo site_url('home/profile/user_credentials'); ?>"><?php echo site_phrase('account'); ?></a></li>
                                <li><a href="<?php echo site_url('home/profile/user_photo'); ?>"><?php echo site_phrase('photo'); ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="user-dashboard-content">
                        <div class="content-title-box">
                            <div class="title"><?php echo site_phrase('account'); ?></div>
                            <div class="subtitle"><?php echo site_phrase('edit_your_account_settings'); ?>.</div>
                        </div>
                        <form action="<?php echo site_url('home/update_profile/update_credentials'); ?>" method="post" class="required-form">
                            <div class="content-box">
                                <div class="email-group">
                                    <div class="form-group">
                                        <label for="email"><?php echo site_phrase('email'); ?>:</label>
                                        <input type="email" class="form-control" name = "email" id="email" placeholder="<?php echo site_phrase('email'); ?>" value="<?php echo $user_details['email']; ?>" required>
                                        <small id="email-help" class="form-text description-error"></small>
                                    </div>
                                </div>
                                <div class="password-group">
                                    <div class="form-group">
                                        <label for="password"><?php echo site_phrase('password'); ?>:</label>
                                        <input type="password" class="form-control" id="current_password" name = "current_password" placeholder="<?php echo site_phrase('enter_current_password'); ?>">
                                        <small id="current_password-help" class="form-text description-error"></small>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="new_password" name ="new_password" placeholder="<?php echo site_phrase('enter_new_password'); ?>">
                                        <small id="new_password-help" class="form-text description-error"></small>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="confirm_password" name ="confirm_password" placeholder="<?php echo site_phrase('re-type_your_password'); ?>">
                                        <small id="confirm_password-help" class="form-text description-error"></small>
                                    </div>
                                </div>
                            </div>
                            <div class="content-update-box">
                                <button type="button" class="btn" onclick="userAccountUpdate()" ><?php echo site_phrase('save'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
function userAccountUpdate() {
    var email = $('form.required-form input[name="email"]').val();
    var current_password = $('form.required-form input[name="current_password"]').val();
    var new_password = $('form.required-form input[name="new_password"]').val();
    var confirm_password = $('form.required-form input[name="confirm_password"]').val();
    var regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,16}$/; //Must contain 8 - 16 letter and number and uppercase
    var regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    var error = 0;
    if(new_password != '' || confirm_password != '' || current_password != ''){
        $('form.required-form').find('input[type="password"]').each(function(){
            let inputId = $(this).attr('id');
            if ($(this).val() === "") {
                error = 1;
                $('#'+inputId).addClass('input-error');
                $('#'+inputId+'-help').html(`<?php echo site_phrase('input_required') ?>`);
            }else{
                $('#'+inputId).removeClass('input-error');
                $('#'+inputId+'-help').html('');
            }
        });
        if(error == 1){
            return;
        }

        if(new_password != confirm_password){
            error = 1;
            $('#confirm_password-help').html('<?php echo site_phrase('error_password_mismatch') ?>');
            $('#confirm_password').addClass('input-error');
        }else{
            $('#confirm_password-help').html('');
            $('#confirm_password').removeClass('input-error');
        }

        if(!regexPassword.test(new_password)){
            error = 1;
            $('#new_password-help').html('<?php echo site_phrase('error_invalid_password') ?>');
            $('#new_password').addClass('input-error');
        }else{
            $('#new_password-help').html('');
            $('#new_password').removeClass('input-error');
        }
    }

    if(email == ''){
        error = 1;
        $('#email-help').html('<?php echo site_phrase('input_required') ?>');
        $('#email').addClass('input-error');
    }else{
        $('#email-help').html('');
        $('#email').removeClass('input-error');
    }

    if(error == 1){
        return;
    }

    if(!regexEmail.test(email)){
        error = 1;
        $('#email-help').html('<?php echo site_phrase('error_invalid_email') ?>');
        $('#email').addClass('input-error');
    }else{
        $('#email-help').html('');
        $('#email').removeClass('input-error');
        $.ajax({
            url: '<?php echo site_url('login/ajax_check_duplicate_email');?>',
            type : 'POST',
            data : {email : email, method: 'on_update'},
            success: function(response)
            {
                response = JSON.parse(response);
                if(response == true){
                    $('#email-help').html('');
                    $('#email').removeClass('input-error');
                    if(error == 0){
                        $('form.required-form').submit();
                    }
                }else{
                    $('#email-help').html('<?php echo site_phrase('email_exist') ?>');
                    $('#email').addClass('input-error');
                }
            }
        });
    }
}
</script>