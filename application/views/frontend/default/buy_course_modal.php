<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="user-dashboard-box mt-3">
                <div class="user-dashboard-content w-100 modal-login-form <?php echo $mt_class; ?>">
                    <div class="content-title-box">
                        <div class="title"><?php echo site_phrase('buy_course_info'); ?></div>
                    </div>
                    <form action="" method="post" id="login_ajax">
                        <div class="content-box">
                            <div class="basic-group">
                                <?php echo get_frontend_settings('buy_course_info') ?>
                            </div>
                        </div>
                        <div class="content-update-box">
                            <button type="button" class="btn" data-dismiss="modal" aria-label="Close">
                                <?php echo site_phrase('close'); ?>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



