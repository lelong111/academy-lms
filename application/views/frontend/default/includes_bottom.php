<script src="<?php echo base_url() . 'assets/frontend/default/js/vendor/modernizr-3.5.0.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'assets/frontend/default/js/vendor/jquery-3.2.1.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'assets/frontend/default/js/popper.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'assets/frontend/default/js/bootstrap.min.js'; ?>"></script>
<script src="<?php echo base_url('assets/frontend/default/js/ckeditor/build/ckeditor.js'); ?>"></script>
<?php if ($page_name == "home") : ?>
	<script src="<?php echo base_url() . 'assets/frontend/default/js/slick.min.js'; ?>"></script>
	<script src="<?php echo base_url() . 'assets/frontend/default/js/jquery.webui-popover.min.js'; ?>"></script>
<?php endif; ?>

<?php if ($page_name == "user_profile") : ?>
	<script src="<?php echo base_url() . 'assets/frontend/default/js/tinymce.min.js'; ?>"></script>
<?php endif; ?>

<script src="<?php echo base_url() . 'assets/frontend/default/js/main.js'; ?>"></script>
<script src="<?php echo base_url() . 'assets/frontend/default/js/custom.js'; ?>"></script>
<script src="<?php echo base_url() . 'assets/global/toastr/toastr.min.js'; ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>

<!-- SHOW TOASTR NOTIFIVATION -->
<?php if ($this->session->flashdata('flash_message') != "") : ?>

	<script type="text/javascript">
		toastr.success('<?php echo $this->session->flashdata("flash_message"); ?>');
	</script>

<?php endif; ?>

<?php if ($this->session->flashdata('error_message') != "") : ?>

	<script type="text/javascript">
		toastr.error('<?php echo $this->session->flashdata("error_message"); ?>');
	</script>

<?php endif; ?>

<?php if ($this->session->flashdata('info_message') != "") : ?>

	<script type="text/javascript">
		toastr.info('<?php echo $this->session->flashdata("info_message"); ?>');
	</script>

<?php endif; ?>

<script>
	function error_required_field() {
		toastr.error("<?php echo get_phrase('please_fill_all_the_required_fields'); ?>", '', {
			timeOut: 3000
		})
	}

	function error_invalid_password() {
		toastr.error("<?php echo get_phrase('error_invalid_password'); ?>", '', {
			timeOut: 3000
		})
	}

	function error_password_mismatch() {
		toastr.error("<?php echo get_phrase('error_password_mismatch'); ?>", '', {
			timeOut: 3000
		})
	}
</script>

<?php if ($this->session->__get('session_alert')) : ?>
	<?php $this->session->__set('session_alert', null); ?>
	<div class="modal fade" id="sessionAcceptModal" tabindex="-1" role="dialog" aria-labelledby="sessionAcceptLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="sessionAcceptLabel"><?php echo get_phrase('warning'); ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<?php echo get_phrase('login_already_in_use'); ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo get_phrase('no'); ?></button>
					<button type="button" class="btn btn-primary" onclick="sessionLogin()"><?php echo get_phrase('yes'); ?></button>
				</div>
			</div>
		</div>
	</div>
	<form action="<?php echo site_url('login/validate_login/session'); ?>" method="post" id="sessionLogin" style="display:none;">
	</form>
	<script type="text/javascript">
		console.log('gà');
		$('#sessionAcceptModal').modal('show');

		function sessionLogin() {
			$('#sessionLogin').submit();
		}
	</script>
<?php endif; ?>