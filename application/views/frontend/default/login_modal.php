<?php if (get_frontend_settings('recaptcha_status')) : ?>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
<?php endif; ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="user-dashboard-box mt-3">
                    <div class="social-custom-form w-100 mt-4">
                        <?php if (get_settings('fb_social_login')) :
                            include "facebook_login.php";
                        endif; ?>
                        <?php if (get_settings('discord_social_login')) :
                            include "discord_login.php";
                        endif; ?>
                    </div>

                    <div class="user-dashboard-content w-100 modal-login-form <?php echo $mt_class; ?>">
                        <div class="content-title-box">
                            <div class="title"><?php echo site_phrase('login'); ?></div>
                            <div class="subtitle"><?php echo site_phrase('provide_your_valid_login_credentials'); ?></div>
                            <small id="error_message" class="form-text description-error"></small>
                        </div>
                        <form action="" method="post" id="login_ajax">
                            <div class="content-box">
                                <div class="basic-group">
                                    <div class="form-group">
                                        <label for="email"><span class="input-field-icon"><i class="fas fa-envelope"></i></span> <?php echo site_phrase('email'); ?>:</label>
                                        <input type="email" class="form-control" name="email" id="modal_email" placeholder="<?php echo site_phrase('email'); ?>" value="" required>
                                        <small id="modal_email-help" class="form-text description-error"></small>
                                    </div>
                                    <div class="form-group">
                                        <label for="password"><span class="input-field-icon"><i class="fas fa-lock"></i></span> <?php echo site_phrase('password'); ?>:</label>
                                        <input type="password" class="form-control" name="password" id="modal_password" placeholder="<?php echo site_phrase('password'); ?>" value="" required>
                                        <small id="modal_password-help" class="form-text description-error"></small>
                                    </div>
                                    <?php if (get_frontend_settings('recaptcha_status')) : ?>
                                        <div class="form-group">
                                            <div class="g-recaptcha" data-sitekey="<?php echo get_frontend_settings('recaptcha_sitekey'); ?>"></div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="content-update-box" style="padding: 10px 27px">
                                <button class="btn" type="button" onclick="modaluserLogin()" style="width:100%"><?php echo site_phrase('login'); ?></button>
                            </div>
                            <div class="content-update-box" style="padding: 0">
                                    <b>Or</b>
                            </div>
                            <div class="content-update-box" style="padding: 10px 27px">
                                <button class="btn" type="button" onclick="loginQmv()" style="width:100%"><?php echo site_phrase('qmv_login'); ?></button>
                            </div>
                            <div class="forgot-pass text-center">
                                <span><?php echo site_phrase('or'); ?></span>
                                <a href="javascript::" onclick="toggoleFormModal('forgot_password')"><?php echo site_phrase('forgot_password'); ?></a>
                            </div>
                            <div class="account-have text-center">
                                <?php echo site_phrase('do_not_have_an_account'); ?>? <a href="<?php echo site_url('home/sign_up'); ?>"><?php echo site_phrase('sign_up'); ?></a>
                            </div>
                        </form>
                    </div>
                    
                    <div class="user-dashboard-content w-100 modal-qmv-login-form hidden <?php echo $mt_class; ?>">
                        <div class="content-title-box">
                            <div class="title"><?php echo site_phrase('qmv_login'); ?></div>
                            <div class="subtitle"><?php echo site_phrase('provide_your_qmv_valid_login_credentials'); ?></div>
                            <small id="error_message_qmv" class="form-text description-error"></small>
                        </div>
                        <form action="" method="post" id="qmv_login_ajax">
                            <div class="content-box">
                                <div class="basic-group">
                                    <div class="form-group">
                                        <label for="qmv_user_name"><span class="input-field-icon"><i class="fas fa-user"></i></span> <?php echo site_phrase('qmv_user_name'); ?>:</label>
                                        <input type="text" class="form-control" name="user_name" id="qmv_user_name" placeholder="<?php echo site_phrase('qmv_user_name'); ?>" value="" required>
                                        <small id="qmv_user_name-help" class="form-text description-error"></small>
                                    </div>
                                    <div class="form-group">
                                        <label for="qmv_password"><span class="input-field-icon"><i class="fas fa-lock"></i></span> <?php echo site_phrase('qmv_password'); ?>:</label>
                                        <input type="password" class="form-control" name="password" id="qmv_password" placeholder="<?php echo site_phrase('qmv_password'); ?>" value="" required>
                                        <small id="qmv_password-help" class="form-text description-error"></small>
                                    </div>
                                    <?php if (get_frontend_settings('recaptcha_status')) : ?>
                                        <div class="form-group">
                                            <div class="g-recaptcha" data-sitekey="<?php echo get_frontend_settings('recaptcha_sitekey'); ?>"></div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="content-update-box" style="padding: 10px 27px">
                                <button class="btn" type="button" onclick="qmvUserLogin()" style="width:100%"><?php echo site_phrase('qmv_login'); ?></button>
                            </div>
                            <div class="content-update-box" style="padding: 0">
                                    <b>Or</b>
                            </div>
                            <div class="content-update-box" style="padding: 10px 27px">
                                <button class="btn" type="button" onclick="normalLogin()" style="width:100%"><?php echo site_phrase('login'); ?></button>
                            </div>
                            <div class="forgot-pass text-center">
                                <span><?php echo site_phrase('or'); ?></span>
                                <a href="javascript::" onclick="toggoleFormModal('forgot_password')"><?php echo site_phrase('forgot_password'); ?></a>
                            </div>
                            <div class="account-have text-center">
                                <?php echo site_phrase('do_not_have_an_account'); ?>? <a href="<?php echo site_url('home/sign_up'); ?>"><?php echo site_phrase('sign_up'); ?></a>
                            </div>
                        </form>
                    </div>

                    <div class="user-dashboard-content w-100 modal-forgot-password-form hidden <?php echo $mt_class; ?>">
                        <div class="content-title-box">
                            <div class="title"><?php echo site_phrase('forgot_password'); ?></div>
                            <div class="subtitle"><?php echo site_phrase('provide_your_email_address_to_get_password'); ?>.</div>
                        </div>
                        <form action="<?php echo site_url('login/forgot_password/frontend'); ?>" method="post" id="modal_forgot_password">
                            <div class="content-box">
                                <div class="basic-group">
                                    <div class="form-group">
                                        <label for="forgot-email"><span class="input-field-icon"><i class="fas fa-envelope"></i></span> <?php echo site_phrase('email'); ?>:</label>
                                        <input type="email" class="form-control" name="email" id="modal_forgot-email" placeholder="<?php echo site_phrase('email'); ?>" value="" required>
                                        <small id="modal_forgot-email-help" class="form-text text-muted"><?php echo site_phrase('provide_your_email_address_to_get_password'); ?>.</small>
                                    </div>
                                    <?php if (get_frontend_settings('recaptcha_status')) : ?>
                                        <div class="form-group">
                                            <div class="g-recaptcha" data-sitekey="<?php echo get_frontend_settings('recaptcha_sitekey'); ?>"></div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="content-update-box">
                                <button class="btn" type="button" onclick="resetPassword()"><?php echo site_phrase('reset_password'); ?></button>
                            </div>
                            <div class="forgot-pass text-center">
                                <?php echo site_phrase('want_to_go_back'); ?>? <a href="javascript::" onclick="toggoleFormModal('login')"><?php echo site_phrase('login'); ?></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form action="<?php echo site_url('login/discord_login'); ?>" method="post" id="discordLogin" style="display: none;">
    </form>

<script type="text/javascript">
    function submitDiscordLogin() {
        $('#discordLogin').submit();
    }
    function toggoleFormModal(form_type) {
        if (form_type === 'login') {
            $('.modal-login-form').show();
            $('.modal-forgot-password-form').hide();
            $('.modal-qmv-login-form').hide();
        } else if (form_type === 'forgot_password') {
            $('.modal-login-form').hide();
            $('.modal-qmv-login-form').hide();
            $('.modal-forgot-password-form').show();
        }
    }

    function normalLogin(){
        $('.modal-forgot-password-form').hide();
        $('.modal-qmv-login-form').hide();
        $('.modal-login-form').show();
    }

    function loginQmv(){
        $('.modal-forgot-password-form').hide();
        $('.modal-login-form').hide();
        $('.modal-qmv-login-form').show();
    }


    function modaluserLogin(){
        var email = $('#modal_email').val();
        var password = $('#modal_password').val();

        let error = 0;
        $('#login_ajax').find('input').each(function() {
            let inputId = $(this).attr('id');
            if ($(this).val() === "") {
                error = 1;
                $('#' + inputId).addClass('input-error');
                $('#' + inputId + '-help').html(`<?php echo site_phrase('input_required') ?>`);
            } else {
                $('#' + inputId).removeClass('input-error');
                $('#' + inputId + '-help').html('');
            }
        });

        if (error == 1) {
            return
        }

        $.ajax({
            url: '<?php echo site_url('login/ajax_login'); ?>',
            type: 'POST',
            data: {
                email: email,
                password: password
            },
            success: function(response) {
                let res = JSON.parse(response);
                if(res.status == 'error'){
                    $('#error_message').html(res.message);
                }
                if(res.status == 'success'){
                    if(res.is_admin){
                        location.href = '<?php echo site_url('admin/dashboard') ?>';
                    }else{
                        location.reload();
                    }
                }
            }
        });

    }
    function resetPassword() {
        var email = $('#modal_forgot_password').find('input[name="email"]').val();
        if (email == '') {
            $('#modal_forgot-email-help').addClass('description-error');
            $('#modal_forgot-email-help').html('<?php echo site_phrase('input_required') ?>');
            $('#modal_forgot-email').addClass('input-error');
            return;
        }

        $.ajax({
            url: '<?php echo site_url('login/ajax_check_email_exist'); ?>',
            type: 'POST',
            data: {
                email: email
            },
            success: function(response) {
                response = JSON.parse(response);
                if (response == true) {
                    $('#modal_forgot_password').submit();
                } else {
                    $('#modal_forgot-email-help').html('<?php echo site_phrase('email_not_exist') ?>');
                    $('#modal_forgot-email').addClass('input-error');
                    $('#modal_forgot-email-help').addClass('description-error');
                }
            }
        });
    }

    function qmvUserLogin(){
        var userName = $('#qmv_user_name').val();
        var password = $('#qmv_password').val();

        let error = 0;
        $('#qmv_login_ajax').find('input').each(function() {
            let inputId = $(this).attr('id');
            if ($(this).val() === "") {
                error = 1;
                $('#' + inputId).addClass('input-error');
                $('#' + inputId + '-help').html(`<?php echo site_phrase('input_required') ?>`);
            } else {
                $('#' + inputId).removeClass('input-error');
                $('#' + inputId + '-help').html('');
            }
        });

        if (error == 1) {
            return
        }

        $.ajax({
            url: '<?php echo site_url('login/qmv_ajax_login'); ?>',
            type: 'POST',
            data: {
                username: userName,
                password: password
            },
            success: function(response) {
                let res = JSON.parse(response);
                if(res.status == 'error'){
                    $('#error_message_qmv').html(res.message);
                }
                if(res.status == 'success'){
                    location.reload();
                }
            }
        });
    }
</script>
