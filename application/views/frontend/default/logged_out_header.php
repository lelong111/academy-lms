<section class="menu-area">
  <div class="container-xl">
    <div class="row">
      <div class="col">
        <nav class="navbar navbar-expand-lg navbar-light">

          <ul class="mobile-header-buttons">
            <li><a class="mobile-nav-trigger" href="#mobile-primary-nav">Menu<span></span></a></li>
            <li style="padding-top: 3px;"><button type="button" class="btn btn-sign-in" data-toggle="modal" data-target="#signInModal"><?php echo site_phrase('log_in'); ?></button></li>
          </ul>

          <a href="<?php echo site_url(''); ?>" class="navbar-brand" href="#"><img src="<?php echo base_url('uploads/system/'.get_frontend_settings('dark_logo')); ?>" alt="" height="35"></a>

          <?php include 'menu.php'; ?>

          <form class="inline-form search-form" action="<?php echo site_url('home/search'); ?>" method="get" style="width: 100%;">
            <div class="input-group search-box mobile-search">
              <input type="text" name = 'query' class="form-control" placeholder="<?php echo site_phrase('search_for_courses'); ?>">
              <div class="input-group-append">
                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </form>

          <?php if ($this->session->userdata('admin_login')): ?>
            <div class="instructor-box menu-icon-box">
              <div class="icon">
                <a href="<?php echo site_url('admin'); ?>" style="border: 1px solid transparent; margin: 10px 10px; font-size: 14px; width: 100%; border-radius: 0;"><?php echo site_phrase('administrator'); ?></a>
              </div>
            </div>
            
          
          <?php if(get_frontend_settings('cart_status') == 'active') : ?>
          <div class="cart-box menu-icon-box" id = "cart_items">
            <?php include 'cart_items.php'; ?>
          </div>
            <span class="signin-box-move-desktop-helper"></span>
            <div class="sign-in-box btn-group">
              <a href="<?php echo site_url('login/logout'); ?>" class="btn btn-sign-up"><?php echo site_phrase('logout'); ?></a>
            </div>
          <?php endif; ?>
          <?php else: ?>
          <span class="signin-box-move-desktop-helper"></span>
          <div class="sign-in-box btn-group">

            <!-- <a href="<?php //echo site_url('home/login'); ?>" class="btn btn-sign-in"><?php //echo site_phrase('log_in'); ?></a> -->
            <button type="button" class="btn btn-sign-in" data-toggle="modal" data-target="#signInModal"><?php echo site_phrase('log_in'); ?></button>

            <a href="<?php echo site_url('home/sign_up'); ?>" class="btn btn-sign-up"><?php echo site_phrase('sign_up'); ?></a>

          </div> <!--  sign-in-box end -->
          <?php endif; ?>
        </nav>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="signInModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body login_modal">
        <?php include 'login_modal.php'; ?>
      </div>
    </div>
  </div>
</div>