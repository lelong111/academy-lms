<section class="home-banner-area" style="background-image: url('<?= base_url("uploads/system/" . get_frontend_settings('banner_image')); ?>');
        background-position: center center;
        background-size: cover;
        background-repeat: no-repeat;
        padding: 10px 0 10px;
        color: #fff;">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <div class="home-banner-wrap">
                    <h2><?php echo get_frontend_settings('banner_title'); ?></h2>
                    <p><?php echo get_frontend_settings('banner_sub_title'); ?></p>
                    <form class="" action="<?php echo site_url('home/search'); ?>" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" name="query" placeholder="<?php echo site_phrase('what_do_you_want_to_learn'); ?>?">
                            <div class="input-group-append">
                                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="home-fact-area">
    <div class="container-lg">
        <div class="row">
            <?php $courses = $this->crud_model->get_courses(); ?>
            <div class="col-md-4 d-flex">
                <div class="home-fact-box mr-md-auto mr-auto">
                    <i class="fas fa-bullseye float-left"></i>
                    <div class="text-box">
                        <h4><?php
                            $status_wise_courses = $this->crud_model->get_status_wise_courses();
                            $number_of_courses = $status_wise_courses['active']->num_rows();
                            echo $number_of_courses . ' ' . site_phrase('online_courses'); ?></h4>
                        <p><?php echo site_phrase('explore_a_variety_of_fresh_topics'); ?></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 d-flex">
                <div class="home-fact-box mr-md-auto mr-auto">
                    <i class="fa fa-check float-left"></i>
                    <div class="text-box">
                        <h4><?php echo site_phrase('expert_instruction'); ?></h4>
                        <p><?php echo site_phrase('find_the_right_course_for_you'); ?></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 d-flex">
                <div class="home-fact-box mr-md-auto mr-auto">
                    <i class="fa fa-clock float-left"></i>
                    <div class="text-box">
                        <h4><?php echo site_phrase('lifetime_access'); ?></h4>
                        <p><?php echo site_phrase('learn_on_your_schedule'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="course-carousel-area">
    <div class="container-course">
        <div class="row">
            <div class="col">
                <h2 class="course-title-cate"><?php echo site_phrase('top_courses'); ?></h2>
                <div class="category-course-list">
                    <ul>
                    <?php $top_courses = $this->crud_model->get_top_courses()->result_array() ;
                    $sorted_top_course_list = sort_top_course($top_courses);
                    $cart_items = $this->session->userdata('cart_items');
                    foreach ($sorted_top_course_list as $course) : 
                        $instructor_details = $this->user_model->get_all_user($course['user_id'])->row_array();?>
                        <li>
                            <div class="course-box-2">
                                <div class="course-image">
                                    <a href="<?php echo site_url('home/course/' . rawurlencode(slugify($course['title'])) . '/' . $course['id']) ?>">
                                        <img src="<?php echo $this->crud_model->get_course_thumbnail_url($course['id']); ?>" alt="" class="img-fluid" style="height:100%">
                                    </a>
                                </div>
                                <div class="course-details" style="width: 74%;">
                                    <div>
                                        <a href="<?php echo site_url('home/course/' . rawurlencode(slugify($course['title'])) . '/' . $course['id']); ?>" class="course-title"><?php echo $course['title']; ?></a>
                                        <div class="d-flex">
                                            <?php if ($course['multi_instructor']) : ?>
                                                <?php $instructors = $this->user_model->get_multi_instructor_details_with_csv($course['user_id']); ?>
                                                <?php foreach ($instructors as $key => $instructor) : ?>
                                                    <?php echo $key > 0 ? '<span class="course-instructor instructor-name" style="margin-right: 1px;">, </span>' : '' ?>
                                                    <a href="<?php echo site_url('home/instructor_page/' . $instructor['id']) ?>" class="course-instructor">
                                                        <span class="instructor-name"><?php echo $instructor['first_name'] . ' ' . $instructor['last_name']; ?></span>
                                                    </a>
                                                <?php endforeach; ?>
                                            <?php else : ?>
                                                <a href="<?php echo site_url('home/instructor_page/' . $instructor_details['id']) ?>" class="course-instructor">
                                                    <span class="instructor-name"><?php echo $instructor_details['first_name'] . ' ' . $instructor_details['last_name']; ?></span>
                                                </a>
                                            <?php endif; ?>
                                        </div>

                                        <div class="course-subtitle mb-3">
                                            <?php echo $course['short_description']; ?>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="course-meta mr-auto">
                                            <?php if ($course['course_type'] == 'general') : ?>
                                                <span class=""><i class="fas fa-play-circle"></i>
                                                    <?php
                                                    $number_of_lessons = $this->crud_model->get_lessons('course', $course['id'])->num_rows();
                                                    echo $number_of_lessons . ' ' . site_phrase('lessons');
                                                    ?>
                                                </span>
                                                <span class=""><i class="far fa-clock"></i>
                                                    <?php echo $this->crud_model->get_total_duration_of_lesson_by_course_id($course['id']); ?>
                                                </span>
                                            <?php endif; ?>
                                            <span class=""><i class="fas fa-closed-captioning"></i><?php echo site_phrase($course['language']); ?></span>
                                            <span class=""><i class="fa fa-level-up"></i><?php echo site_phrase($course['level']); ?></span>
                                            <p class="text-left text-secondary d-inline-block course-compare" style="font-size: 13px; cursor : pointer; font-weight : 500; color : #4d98ad !important;" redirect_to="<?php echo site_url('home/compare?course-1=' . rawurlencode(slugify($course['title'])) . '&&course-id-1=' . $course['id']); ?>">
                                                <i class="fas fa-balance-scale"></i> <?php echo site_phrase('compare_this_course'); ?>
                                            </p>
                                        </div>
                                        <div class="course-price-rating" style="padding: 0px;">
                                            <div class="course-price">
                                                <?php if ($course['is_free_course'] == 1 || $free_for_org_member) : ?>
                                                    <span class="current-price"><?php echo site_phrase('free'); ?></span>
                                                <?php else : ?>
                                                    <?php if ($course['discount_flag'] == 1) : ?>
                                                        <span class="current-price"><?php echo currency($course['discounted_price']); ?></span>
                                                        <span class="original-price"><?php echo currency($course['price']); ?></span>
                                                    <?php else : ?>
                                                        <span class="current-price"><?php echo currency($course['price']); ?></span>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                            <?php if(get_frontend_settings('feedback_status') == 'active'): ?>
                                            <div class="rating">
                                                <?php
                                                $total_rating =  $this->crud_model->get_ratings('course', $course['id'], true)->row()->rating;
                                                $number_of_ratings = $this->crud_model->get_ratings('course', $course['id'])->num_rows();
                                                if ($number_of_ratings > 0) {
                                                    $average_ceil_rating = ceil($total_rating / $number_of_ratings);
                                                } else {
                                                    $average_ceil_rating = 0;
                                                }

                                                for ($i = 1; $i < 6; $i++) : ?>
                                                    <?php if ($i <= $average_ceil_rating) : ?>
                                                        <i class="fas fa-star filled"></i>
                                                    <?php else : ?>
                                                        <i class="fas fa-star"></i>
                                                    <?php endif; ?>
                                                <?php endfor; ?>
                                                <!-- <span class="d-inline-block average-rating"><?php echo $average_ceil_rating; ?></span> -->
                                            </div>
                                            <div class="rating-number">
                                                <?php echo $this->crud_model->get_ratings('course', $course['id'])->num_rows() . ' ' . site_phrase('ratings'); ?>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="course-carousel-area">
    <div class="container-course">
        <div class="row">
            <div class="col">
                <h2 class="course-title-cate"><?php echo site_phrase('top') . ' 10 ' . site_phrase('latest_courses'); ?></h2>
                <div class="category-course-list">
                    <ul>
                    <?php
                    $latest_courses = $this->crud_model->get_latest_10_course();
                    foreach ($latest_courses as $course) : 
                        $instructor_details = $this->user_model->get_all_user($course['user_id'])->row_array();?>
                        <li>
                            <div class="course-box-2">
                                <div class="course-image">
                                    <a href="<?php echo site_url('home/course/' . rawurlencode(slugify($course['title'])) . '/' . $course['id']) ?>">
                                        <img src="<?php echo $this->crud_model->get_course_thumbnail_url($course['id']); ?>" alt="" class="img-fluid" style="height:100%">
                                    </a>
                                </div>
                                <div class="course-details" style="width: 74%;">
                                    <div>
                                        <a href="<?php echo site_url('home/course/' . rawurlencode(slugify($course['title'])) . '/' . $course['id']); ?>" class="course-title"><?php echo $course['title']; ?></a>
                                        <div class="d-flex">
                                            <?php if ($course['multi_instructor']) : ?>
                                                <?php $instructors = $this->user_model->get_multi_instructor_details_with_csv($course['user_id']); ?>
                                                <?php foreach ($instructors as $key => $instructor) : ?>
                                                    <?php echo $key > 0 ? '<span class="course-instructor instructor-name" style="margin-right: 1px;">, </span>' : '' ?>
                                                    <a href="<?php echo site_url('home/instructor_page/' . $instructor['id']) ?>" class="course-instructor">
                                                        <span class="instructor-name"><?php echo $instructor['first_name'] . ' ' . $instructor['last_name']; ?></span>
                                                    </a>
                                                <?php endforeach; ?>
                                            <?php else : ?>
                                                <a href="<?php echo site_url('home/instructor_page/' . $instructor_details['id']) ?>" class="course-instructor">
                                                    <span class="instructor-name"><?php echo $instructor_details['first_name'] . ' ' . $instructor_details['last_name']; ?></span>
                                                </a>
                                            <?php endif; ?>
                                        </div>

                                        <div class="course-subtitle mb-3">
                                            <?php echo $course['short_description']; ?>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="course-meta mr-auto">
                                            <?php if ($course['course_type'] == 'general') : ?>
                                                <span class=""><i class="fas fa-play-circle"></i>
                                                    <?php
                                                    $number_of_lessons = $this->crud_model->get_lessons('course', $course['id'])->num_rows();
                                                    echo $number_of_lessons . ' ' . site_phrase('lessons');
                                                    ?>
                                                </span>
                                                <span class=""><i class="far fa-clock"></i>
                                                    <?php echo $this->crud_model->get_total_duration_of_lesson_by_course_id($course['id']); ?>
                                                </span>
                                            <?php endif; ?>
                                            <span class=""><i class="fas fa-closed-captioning"></i><?php echo site_phrase($course['language']); ?></span>
                                            <span class=""><i class="fa fa-level-up"></i><?php echo site_phrase($course['level']); ?></span>
                                            <p class="text-left text-secondary d-inline-block course-compare" style="font-size: 13px; cursor : pointer; font-weight : 500; color : #4d98ad !important;" redirect_to="<?php echo site_url('home/compare?course-1=' . rawurlencode(slugify($course['title'])) . '&&course-id-1=' . $course['id']); ?>">
                                                <i class="fas fa-balance-scale"></i> <?php echo site_phrase('compare_this_course'); ?>
                                            </p>
                                        </div>
                                        <div class="course-price-rating" style="padding: 0px;">
                                            <div class="course-price">
                                                <?php if ($course['is_free_course'] == 1 || $free_for_org_member) : ?>
                                                    <span class="current-price"><?php echo site_phrase('free'); ?></span>
                                                <?php else : ?>
                                                    <?php if ($course['discount_flag'] == 1) : ?>
                                                        <span class="current-price"><?php echo currency($course['discounted_price']); ?></span>
                                                        <span class="original-price"><?php echo currency($course['price']); ?></span>
                                                    <?php else : ?>
                                                        <span class="current-price"><?php echo currency($course['price']); ?></span>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                            <?php if(get_frontend_settings('feedback_status') == 'active'): ?>
                                            <div class="rating">
                                                <?php
                                                $total_rating =  $this->crud_model->get_ratings('course', $course['id'], true)->row()->rating;
                                                $number_of_ratings = $this->crud_model->get_ratings('course', $course['id'])->num_rows();
                                                if ($number_of_ratings > 0) {
                                                    $average_ceil_rating = ceil($total_rating / $number_of_ratings);
                                                } else {
                                                    $average_ceil_rating = 0;
                                                }

                                                for ($i = 1; $i < 6; $i++) : ?>
                                                    <?php if ($i <= $average_ceil_rating) : ?>
                                                        <i class="fas fa-star filled"></i>
                                                    <?php else : ?>
                                                        <i class="fas fa-star"></i>
                                                    <?php endif; ?>
                                                <?php endfor; ?>
                                                <!-- <span class="d-inline-block average-rating"><?php echo $average_ceil_rating; ?></span> -->
                                            </div>
                                            <div class="rating-number">
                                                <?php echo $this->crud_model->get_ratings('course', $course['id'])->num_rows() . ' ' . site_phrase('ratings'); ?>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function handleWishList(elem) {

        $.ajax({
            url: '<?php echo site_url('home/handleWishList'); ?>',
            type: 'POST',
            data: {
                course_id: elem.id
            },
            success: function(response) {
                if (!response) {
                    window.location.replace("<?php echo site_url('login'); ?>");
                } else {
                    if ($(elem).hasClass('active')) {
                        $(elem).removeClass('active')
                    } else {
                        $(elem).addClass('active')
                    }
                    $('#wishlist_items').html(response);
                }
            }
        });
    }

    function handleCartItems(elem) {
        url1 = '<?php echo site_url('home/handleCartItems'); ?>';
        url2 = '<?php echo site_url('home/refreshWishList'); ?>';
        $.ajax({
            url: url1,
            type: 'POST',
            data: {
                course_id: elem.id
            },
            success: function(response) {
                $('#cart_items').html(response);
                if ($(elem).hasClass('addedToCart')) {
                    $('.big-cart-button-' + elem.id).removeClass('addedToCart')
                    $('.big-cart-button-' + elem.id).text("<?php echo site_phrase('add_to_cart'); ?>");
                } else {
                    $('.big-cart-button-' + elem.id).addClass('addedToCart')
                    $('.big-cart-button-' + elem.id).text("<?php echo site_phrase('added_to_cart'); ?>");
                }
                $.ajax({
                    url: url2,
                    type: 'POST',
                    success: function(response) {
                        $('#wishlist_items').html(response);
                    }
                });
            }
        });
    }

    function handleEnrolledButton() {
        $.ajax({
            url: '<?php echo site_url('home/isLoggedIn'); ?>',
            success: function(response) {
                if (!response) {
                    window.location.replace("<?php echo site_url('login'); ?>");
                }
            }
        });
    }

    $(document).ready(function() {
        if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            if ($(window).width() >= 840) {
                $('a.has-popover').webuiPopover({
                    trigger: 'hover',
                    animation: 'pop',
                    placement: 'horizontal',
                    delay: {
                        show: 500,
                        hide: null
                    },
                    width: 330
                });
            } else {
                $('a.has-popover').webuiPopover({
                    trigger: 'hover',
                    animation: 'pop',
                    placement: 'vertical',
                    delay: {
                        show: 100,
                        hide: null
                    },
                    width: 335
                });
            }
        }
    });

    $('.course-compare').click(function(e) {
        e.preventDefault()
        var redirect_to = $(this).attr('redirect_to');
        window.location.replace(redirect_to);
    });
</script>