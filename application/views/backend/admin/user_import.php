<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/xlsx.full.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/jszip.js"></script>
<script type="text/javascript">
    function Upload() {
        //Reference the FileUpload element.
        var fileUpload = document.getElementById("upload_excel_file");
 
        //Validate whether File is valid Excel file.
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
        if (regex.test(fileUpload.value.toLowerCase())) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
 
                //For Browsers other than IE.
                if (reader.readAsBinaryString) {
                    reader.onload = function (e) {
                        ProcessExcel(e.target.result);
                    };
                    reader.readAsBinaryString(fileUpload.files[0]);
                } else {
                    //For IE Browser.
                    reader.onload = function (e) {
                        var data = "";
                        var bytes = new Uint8Array(e.target.result);
                        for (var i = 0; i < bytes.byteLength; i++) {
                            data += String.fromCharCode(bytes[i]);
                        }
                        ProcessExcel(data);
                    };
                    reader.readAsArrayBuffer(fileUpload.files[0]);
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {
            alert("Please upload a valid Excel file.");
        }
    };
    function ProcessExcel(data) {
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });
 
        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];
 
        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
        //Set data to form
        if(excelRows.length){
            $("#data_excel_file").val(JSON.stringify(excelRows))
        }
    };
</script>
<?php
    $organizationsArr = [];
    if($organizations){
        $organizationsArr = $organizations->result_array();
    }
?>
<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i>
                    <?php echo $page_title; ?> </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title mb-3"><?php echo get_phrase('import_new_student'); ?></h4>

                <form class="required-form"
                    action="<?php echo site_url('admin/users/import'); ?>"
                    enctype="multipart/form-data" method="post">
                    <div class="tab-content b-0 mb-0">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label"> <?php echo get_phrase('upload_excel_file'); ?> <span class="required">*</span></label>
                                    <div class="input-group col-md-9">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="upload_excel_file" onchange="changeTitleOfImageUploader(this);Upload()"  required accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                            <label class="custom-file-label" for="upload_excel_file"><?php echo get_phrase('select_upload_excel_file'); ?></label>
                                            <input type="hidden" id="data_excel_file" name="data_excel_file">
                                        </div>
                                    </div>
                                    <div class="col-md-9 offset-md-3">
                                        <small class="badge badge-primary"><?php echo 'maximum_upload_size'; ?>: <?php echo ini_get('upload_max_filesize'); ?></small>
                                        <small class="badge badge-primary"><?php echo 'post_max_size'; ?>: <?php echo ini_get('post_max_size'); ?></small>
                                        <small class="badge badge-secondary"><?php echo '"post_max_size" '.get_phrase("has_to_be_bigger_than").' "upload_max_filesize"'; ?></small>
                                    </div>  
                                </div>
                              
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label" for="organization_id"><?php echo get_phrase('organization'); ?></label>
                                        <div class="col-md-9">
                                            <select class="form-control select2" data-toggle="select2" name="organization_id" id="organization_id">
                                                <option value=""><?php echo get_phrase('select_organization'); ?></option>
                                                <?php foreach ($organizationsArr as $organization): ?>
                                                    <option value="<?php echo $organization['id']; ?>"><?php echo $organization['name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label"
                                        for="password"><?php echo site_phrase('password'); ?> <span
                                            class="required">*</span> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="password" name="password" required>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

                        <ul class="list-inline mb-0 wizard">
                            <li class="next list-inline-item float-right">
                                <button type="button" class="btn btn-primary" onclick="checkRequiredFields()"
                                    name="button"><?php echo get_phrase('submit'); ?></button>
                            </li>
                        </ul>

                    </div> <!-- tab-content -->
            </div> <!-- end #progressbarwizard-->
            </form>

        </div> <!-- end card-body -->
    </div> <!-- end card-->
</div>