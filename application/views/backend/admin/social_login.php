<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('social_login_configuration'); ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <?php if (strpos(site_url(), 'https') !== 0) : ?>
                    <div class="alert alert-danger">
                        <strong><?php echo  'SSL(https) ' . get_phrase('issue'); ?> !</strong>
                        <br>
                        <?php echo get_phrase('you_must_use_an_SSL_supported_server_to_use_the_Facebook_login_feature'); ?>
                    </div>
                <?php endif; ?>

                <div class="col-lg-12">
                    <h4 class="mb-3 header-title">
                        <?php echo get_phrase('facebook_login'); ?>
                        <a target="_blank" href="https://developers.facebook.com/docs/development/create-an-app/"><i class="mdi mdi-information-outline" data-toggle="tooltip" data-placement="top" title="<?php echo get_phrase('facebook_app_creation_instruction'); ?>"></i></a>
                    </h4>

                    <form class="required-form" action="<?php echo site_url('admin/social_login_settings/facebook'); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="smtp_protocol"><?php echo get_phrase('facebook_login'); ?><span class="required">*</span></label>
                            <br>
                            <input type="radio" id="fb_social_login_active" name="fb_social_login" value="1" <?php if (get_settings('fb_social_login') == 1) echo 'checked'; ?>>
                            <label for="fb_social_login_active"><?php echo get_phrase('active'); ?></label>

                            <input type="radio" id="fb_social_login_disabled" name="fb_social_login" value="0" <?php if (get_settings('fb_social_login') != 1) echo 'checked'; ?>>
                            <label for="fb_social_login_disabled"><?php echo get_phrase('inactive'); ?></label>
                        </div>

                        <div class="form-group">
                            <label for="fb_app_id"><?php echo get_phrase('facebook_app_id'); ?><span class="required">*</span></label>
                            <input type="text" name="fb_app_id" id="fb_app_id" class="form-control" value="<?php echo get_settings('fb_app_id');  ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="fb_app_secret"><?php echo get_phrase('facebook_app_secret'); ?><span class="required">*</span></label>
                            <input type="text" name="fb_app_secret" id="fb_app_secret" class="form-control" value="<?php echo get_settings('fb_app_secret');  ?>" required>
                        </div>

                        <button type="submit" class="btn btn-primary"><?php echo get_phrase('save_changes'); ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <div class="col-lg-12">
                    <h4 class="mb-3 header-title">
                        <?php echo get_phrase('discord_login'); ?>
                        <a target="_blank" href="https://discord.com/developers/docs/intro#bots-and-apps"><i class="mdi mdi-information-outline" data-toggle="tooltip" data-placement="top" title="<?php echo get_phrase('discord_bot_application_creation_instruction'); ?>"></i></a>
                    </h4>

                    <form class="required-form" action="<?php echo site_url('admin/social_login_settings/discord'); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="smtp_protocol"><?php echo get_phrase('discord_login'); ?><span class="required">*</span></label>
                            <br>
                            <input type="radio" id="discord_social_login_active" name="discord_social_login" value="1" <?php if (get_settings('discord_social_login') == 1) echo 'checked'; ?>>
                            <label for="discord_social_login_active"><?php echo get_phrase('active'); ?></label>

                            <input type="radio" id="discord_social_login_disabled" name="discord_social_login" value="0" <?php if (get_settings('discord_social_login') != 1) echo 'checked'; ?>>
                            <label for="discord_social_login_disabled"><?php echo get_phrase('inactive'); ?></label>
                        </div>

                        <div class="form-group">
                            <label for="discord_client_id"><?php echo get_phrase('discord_client_id'); ?><span class="required">*</span></label>
                            <input type="text" name="discord_client_id" id="discord_client_id" class="form-control" value="<?php echo get_settings('discord_client_id');  ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="discord_client_secret"><?php echo get_phrase('discord_client_secret'); ?><span class="required">*</span></label>
                            <input type="text" name="discord_client_secret" id="discord_client_secret" class="form-control" value="<?php echo get_settings('discord_client_secret');  ?>" required>
                        </div>

                        <div>
                            <div class="form-group">
                                <label for="guild_ids_are_allowed_access_to_website"><?php echo get_phrase('guild_ids_are_allowed_access_to_website'); ?></label>
                            </div>
                            <div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="min-width: 150px;">Guild Id<span class="required">*</span></th>
                                                <th scope="col" style="min-width: 150px;"><?php echo get_phrase('organization'); ?></th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="guild_ids">
                                            <?php
                                            $guildIds = get_settings('guild_ids_are_allowed_access_to_website');
                                            if (!empty($guildIds) && !empty($guildIds = json_decode($guildIds, true))) :;
                                            ?>
                                                <?php foreach ($guildIds as $guildIdKey => $guildId) : ?>
                                                    <tr>
                                                        <td scope="row">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" value="<?php echo $guildId['guild_id']; ?>" name="guild_ids_are_allowed_access_to_website[]" id="guild_ids_are_allowed_access_to_website" placeholder="<?php echo get_phrase('guild_ids'); ?>">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <select class="form-control select2" data-toggle="select2" name="organization_id[]" id="organization_id">
                                                                <option value=""><?php echo get_phrase('select_organization'); ?></option>
                                                                <?php foreach ($organizations->result_array() as $organization) : ?>
                                                                    <option value="<?php echo $organization['id']; ?>" <?php echo $guildId['organization_id'] == $organization['id'] ? "selected" : ""; ?>><?php echo $organization['name']; ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <?php if ($guildIdKey == 0) : ?>
                                                                <button type="button" class="btn btn-success btn-sm" name="button" onclick="appendGuildIds()"> <i class="fa fa-plus"></i> </button>
                                                            <?php else : ?>
                                                                <button type="button" class="btn btn-danger btn-sm" style="margin-top: 0px;" name="button" onclick="removeGuildIds(this)"> <i class="fa fa-minus"></i> </button>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php else : ?>
                                                <tr>
                                                    <td scope="row">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="guild_ids_are_allowed_access_to_website[]" id="guild_ids_are_allowed_access_to_website" placeholder="<?php echo get_phrase('guild_ids'); ?>">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <select class="form-control select2" data-toggle="select2" name="organization_id[]" id="organization_id">
                                                            <option value=""><?php echo get_phrase('select_organization'); ?></option>
                                                            <?php foreach ($organizations->result_array() as $organization) : ?>
                                                                <option value="<?php echo $organization['id']; ?>"><?php echo $organization['name']; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-success btn-sm" name="button" onclick="appendGuildIds()"> <i class="fa fa-plus"></i> </button>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                            <tr id="blank_guild_ids_field">
                                                <td scope="row">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="guild_ids_are_allowed_access_to_website[]" id="guild_ids_are_allowed_access_to_website" placeholder="<?php echo get_phrase('guild_ids'); ?>">
                                                    </div>
                                                </td>
                                                <td>
                                                    <select class="form-control select2" data-toggle="select2" name="organization_id[]" id="organization_id">
                                                        <option value=""><?php echo get_phrase('select_organization'); ?></option>
                                                        <?php foreach ($organizations->result_array() as $organization) : ?>
                                                            <option value="<?php echo $organization['id']; ?>"><?php echo $organization['name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-sm" style="margin-top: 0px;" name="button" onclick="removeGuildIds(this)"> <i class="fa fa-minus"></i> </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary"><?php echo get_phrase('save_changes'); ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var blank_guild_ids = jQuery('#blank_guild_ids_field').html();
    jQuery(document).ready(function() {
        jQuery('#blank_guild_ids_field').hide();
    });

    function appendGuildIds() {
        jQuery('#guild_ids').append(`<tr>${blank_guild_ids}</tr>`);
    }

    function removeGuildIds(guildIdsElem) {
        jQuery(guildIdsElem).parent().parent().remove();
    }
</script>