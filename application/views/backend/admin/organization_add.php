<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i>
                    <?php echo $page_title; ?> </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title mb-3"><?php echo get_phrase('organization_add_form'); ?></h4>

                <form class="required-form"
                    action="<?php echo site_url('admin/organizations/add/'.$organization_id); ?>"
                    enctype="multipart/form-data" method="post">
                    <div class="tab-content b-0 mb-0">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label" for="name"><?php echo get_phrase('name'); ?>
                                        <span class="required">*</span> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="name" name="name"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label class="col-md-3 col-form-label"
                                        for="abbreviation"><?php echo get_phrase('abbreviation'); ?> <span
                                            class="required">*</span> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="abbreviation" name="abbreviation"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <div class="offset-md-3 col-md-9">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="active"
                                                id="active" value="1"
                                                onclick="togglePriceFields(this.id)">
                                            <label class="custom-control-label"
                                                for="active"><?php echo get_phrase('check_if_this_is_active'); ?></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <div class="offset-md-3 col-md-9">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input"
                                                name="free_for_org_member" id="free_for_org_member" value="1"
                                                onclick="togglePriceFields(this.id)">
                                            <label class="custom-control-label"
                                                for="free_for_org_member"><?php echo get_phrase('check_if_this_is_a_free_for_organization_member'); ?></label>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

                        <ul class="list-inline mb-0 wizard">
                            <li class="next list-inline-item float-right">
                                <button type="button" class="btn btn-primary" onclick="checkRequiredFields()"
                                    name="button"><?php echo get_phrase('submit'); ?></button>
                            </li>
                        </ul>

                    </div> <!-- tab-content -->
            </div> <!-- end #progressbarwizard-->
            </form>

        </div> <!-- end card-body -->
    </div> <!-- end card-->
</div>