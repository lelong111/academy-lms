<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('discord_role_configuration'); ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
<div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <div class="col-lg-12">

                    <form class="required-form" action="<?php echo site_url('admin/discord_role_settings/discord'); ?>" method="post" enctype="multipart/form-data">
                        <div>
                            <div class="form-group">
                                <label for="qmv_login_api"><?php echo get_phrase('qmv_login_api'); ?></label>
                            </div>
                            <div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="qmv_login_api" placeholder="qmv login api" name="qmv_login_api" value="<?php echo get_settings('qmv_login_api') ?>">
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="form-group">
                                <label for="qmv_auth_code"><?php echo get_phrase('qmv_auth_code'); ?></label>
                            </div>
                            <div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="qmv_auth_code" placeholder="qmv auth code" name="qmv_auth_code" value="<?php echo get_settings('qmv_auth_code') ?>">
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="form-group">
                                <label><?php echo get_phrase('discord_organization_use_member'); ?></label>
                            </div>
                            <div>
                                <?php $organization_id = get_settings('organization_id_allow'); ?>
                                <select class="form-control select2" data-toggle="select2" name="organization_id" id="organization_id">
                                    <option value=""><?php echo get_phrase('select_organization'); ?></option>
                                    <?php foreach ($organizations->result_array() as $organization) : ?>
                                        <option value="<?php echo $organization['id']; ?>" <?php echo $organization_id == $organization['id'] ? "selected" : ""; ?>><?php echo $organization['name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div>
                            <div class="form-group mt-3">
                                <label><?php echo get_phrase('discord_role_allowed_access_to_all_video'); ?></label>
                            </div>
                            <div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="min-width: 150px;"><?php echo get_phrase('role_name'); ?></th>
                                                <th scope="col" style="min-width: 150px;"><?php echo get_phrase('role_id'); ?></th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="guild_ids">
                                            <?php
                                            $discordRoles = get_settings('role_id_allowed_access_video');
                                            if (!empty($discordRoles) && !empty($discordRoles = json_decode($discordRoles, true))) :;
                                            ?>
                                                <?php foreach ($discordRoles as $discordRoleKey => $discordRole) : ?>
                                                    <tr>
                                                        <td scope="row">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" value="<?php echo $discordRole['role_name']; ?>" name="role_name_allowed_access_video[]" placeholder="<?php echo get_phrase('role_name'); ?>">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" value="<?php echo $discordRole['role_id']; ?>" name="role_id_allowed_access_video[]" placeholder="<?php echo get_phrase('role_id'); ?>">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <?php if ($discordRoleKey == 0) : ?>
                                                                <button type="button" class="btn btn-success btn-sm" name="button" onclick="appendGuildIds()"> <i class="fa fa-plus"></i> </button>
                                                            <?php else : ?>
                                                                <button type="button" class="btn btn-danger btn-sm" style="margin-top: 0px;" name="button" onclick="removeGuildIds(this)"> <i class="fa fa-minus"></i> </button>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php else : ?>
                                                <tr>
                                                    <td scope="row">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="role_name_allowed_access_video[]" placeholder="<?php echo get_phrase('role_name'); ?>">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="role_id_allowed_access_video[]" placeholder="<?php echo get_phrase('role_id'); ?>">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-success btn-sm" name="button" onclick="appendGuildIds()"> <i class="fa fa-plus"></i> </button>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                            <tr id="blank_guild_ids_field">
                                                <td scope="row">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="role_name_allowed_access_video[]" placeholder="<?php echo get_phrase('role_name'); ?>">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="role_id_allowed_access_video[]" placeholder="<?php echo get_phrase('role_id'); ?>">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-sm" style="margin-top: 0px;" name="button" onclick="removeGuildIds(this)"> <i class="fa fa-minus"></i> </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary"><?php echo get_phrase('save_changes'); ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var blank_guild_ids = jQuery('#blank_guild_ids_field').html();
    jQuery(document).ready(function() {
        jQuery('#blank_guild_ids_field').hide();
    });

    function appendGuildIds() {
        jQuery('#guild_ids').append(`<tr>${blank_guild_ids}</tr>`);
    }

    function removeGuildIds(guildIdsElem) {
        jQuery(guildIdsElem).parent().parent().remove();
    }
</script>