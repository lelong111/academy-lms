<?php
    $top_course_list = $this->db->get_where('course',['is_top_course' => 1])->result_array();
    $sorted_top_course_list = sort_top_course($top_course_list);
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row" id = "parent-div" data-plugin="dragula" data-containers='["lesson-list"]'>
                    <div class="col-md-12">
                        <div class="bg-dragula p-2 p-lg-4">
                            <h5 class="mt-0"><?php echo get_phrase('top_course_sort'); ?>
                                <button type="button" class="btn btn-outline-primary btn-sm btn-rounded alignToTitle" id = "lesson-sort-btn" onclick="sort()" name="button"><?php echo get_phrase('update_sorting'); ?></button>
                            </h5>
                            <div id="lesson-list" class="py-2">
                                <?php foreach ($sorted_top_course_list as $top_course): ?>
                                    <!-- item -->
                                    <div class="card mb-0 mt-2 draggable-item" id = "<?php echo $top_course['id']; ?>">
                                        <div class="card-body">
                                            <div class="media">
                                                <div class="media-body">
                                                    <h5 class="mb-1 mt-0"><?php echo $top_course['title']; ?></h5>
                                                </div> <!-- end media-body -->
                                            </div> <!-- end media -->
                                        </div> <!-- end card-body -->
                                    </div> <!-- end col -->
                                    <!-- item ends -->
                                <?php endforeach; ?>
                            </div> <!-- end company-list-1-->
                        </div> <!-- end div.bg-light-->
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div> <!-- end col -->
</div>

<!-- Init Dragula -->
<script type="text/javascript">
function sort() {
    var containerArray = ['lesson-list'];
    var itemArray = [];
    var itemJSON;
    for(var i = 0; i < containerArray.length; i++) {
        $('#'+containerArray[i]).each(function () {
            $(this).find('.draggable-item').each(function() {
                itemArray.push(this.id);
            });
        });
    }

    itemJSON = JSON.stringify(itemArray);
    $.ajax({
        url: '<?php echo site_url('admin/ajax_sort_course/');?>',
        type : 'POST',
        data : {itemJSON : itemJSON},
        success: function(response)
        {
            success_notify('<?php echo get_phrase('course_have_been_sorted'); ?>');
        }
    });
}
</script>
