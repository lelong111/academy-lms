<?php
// $param2 IS FOR COURSE ID AND $param3 IS FOR LESSON TYPE
$course_details = $this->crud_model->get_course_by_id($param2)->row_array();
$sections = $this->crud_model->get_section('course', $param2)->result_array();
?>
<!-- SHOWING THE LESSON TYPE IN AN ALERT VIEW -->
<div class="alert alert-info" role="alert">
    <?php echo get_phrase("lesson_type"); ?> :
    <strong>
        <?php
        if ($param3 == 'html5') {
            echo get_phrase("video_url").' [.mp4]';
        }elseif ($param3 == 'video') {
            echo get_phrase("video_file");
        }elseif ($param3 == 'youtube' || $param3 == 'vimeo') {
            echo get_phrase($param3).' '.get_phrase("video");
        }else{
            echo get_phrase($param3);
        }
        ?>.
    </strong>

    <strong><a href="#" class="ml-1" data-toggle="modal" data-dismiss="modal" onclick="showAjaxModal('<?php echo site_url('modal/popup/lesson_types/'.$param2.'/'.$param3); ?>', '<?php echo get_phrase('add_new_lesson'); ?>')"><?php echo get_phrase("change"); ?></a></strong>
</div>

<!-- ACTUAL LESSON ADDING FORM -->
<form action="<?php echo site_url('admin/lessons/'.$param2.'/add'); ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="course_id" value="<?php echo $param2; ?>">
    <div class="form-group">
        <label><?php echo get_phrase('title'); ?><span class="required">*</span></label>
        <input type="text" name = "title" class="form-control" required>
    </div>

    <div class="form-group">
        <label><?php echo get_phrase('section'); ?></label>
        <select class="form-control select2" data-toggle="select2" name="section_id" required>
            <?php foreach ($sections as $section): ?>
                <option value="<?php echo $section['id']; ?>"><?php echo $section['title']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <?php if ($param3 == 'youtube'): include('youtube_type_lesson_add.php'); endif; ?>
    <?php if ($param3 == 'vimeo'): include('vimeo_type_lesson_add.php'); endif; ?>
    <?php if ($param3 == 'html5'): include('html5_type_lesson_add.php'); endif; ?>
    <?php if ($param3 == 'video'): include('video_type_lesson_add.php'); endif; ?>
    <?php if ($param3 == 'amazon-s3' && addon_status('amazon-s3')): include('amazon_s3_type_lesson_add.php'); endif; ?>
    <?php if ($param3 == 'google_drive_video'): include('google_drive_video_lesson_add.php'); endif; ?>
    <?php if ($param3 == 'document'): include('document_type_lesson_add.php'); endif; ?>
    <?php if ($param3 == 'text'): include('text_type_lesson_add.php'); endif; ?>
    <?php if ($param3 == 'image'): include('image_type_lesson_add.php'); endif; ?>
    <?php if ($param3 == 'iframe'): include('iframe_type_lesson_add.php'); endif; ?>
    <?php if ($param3 == 'audio'): include('audio_type_lesson_add.php'); endif; ?>

    <div class="form-group">
        <label><?php echo get_phrase('summary'); ?></label>
        <textarea name="summary" class="form-control"></textarea>
    </div>

    <div class="form-group">
        <label><?php echo get_phrase('do_you_want_to_keep_it_free_as_a_preview_lesson'); ?>?</label>
        <br>
        <input type="checkbox" name="free_lesson" id="free_lesson" value="1">
        <label for="free_lesson"><?php echo get_phrase('mark_as_free_lesson'); ?></label>
    </div>

    <div class="text-center">
        <div id="bar" class="progress mb-3" style="height: 7px;display:none">
            <div class="bar progress-bar progress-bar-striped progress-bar-animated bg-success" style="width: 0%;"></div>
        </div>
        <button class = "btn btn-success" type="submit" name="button"><?php echo get_phrase('add_lesson'); ?></button>
    </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    initSelect2(['#section_id','#lesson_type', '#lesson_provider', '#lesson_provider_for_mobile_application']);
    initTimepicker();

    // HIDING THE SEARCHBOX FROM SELECT2
    $('select').select2({
        minimumResultsForSearch: -1
    });

    $('.modal form').submit(function( event ) {
        event.preventDefault();
        const lesson_type = '<?php echo $param3?>';
        const upload_max_filesize = parseInt('<?php echo ini_get('upload_max_filesize'); ?>');
        if(["video","audio"].includes(lesson_type)){
            $(this).find(":submit").attr("disabled","disabled"); 
            const url = $(this).attr("action");
            if($(`#system_${lesson_type}_file`).length){
                const file = $(`#system_${lesson_type}_file`)[0].files[0];
                const allowedExtensionsRegx = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
                const allowedAudioExtensionsRegx = /(\.MP3|\.WAV|\.AAC|\.WMA|\.MPEG|\.MP4)$/i;
                const allowedVideoExtensionsRegx = /(\.WEBM|\.MP4)$/i;
                const vaild = lesson_type =="video"? allowedVideoExtensionsRegx.test(file.name) : allowedAudioExtensionsRegx.test(file.name);

                if(vaild && file.size < upload_max_filesize * 1024 * 1024 && lesson_type != "video"){
                    let formData = new FormData($(this)[0]);
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        xhr: function () {
                            var fileXhr = $.ajaxSettings.xhr();
                            if (fileXhr.upload) {
                                $(".modal #bar").show()
                                fileXhr.upload.addEventListener("progress", function (e) {
                                    if (e.lengthComputable) {
                                        $('.modal #bar .bar').css('width', `${e.loaded/e.total * 100}%`);
                                        if(e.loaded == e.total){
                                            location.reload();
                                        } 
                                    }
                                }, false);
                            }
                            return fileXhr;
                        }
                    });
                    return false;
                }

                if(vaild && file.size < upload_max_filesize * 1024 * 1024 && lesson_type == "video"){
                    var BYTES_PER_CHUNK = parseInt(100*1024*1024, 10),
                    size = file.size,
                    type = file.type,
                    NUM_CHUNKS = Math.max(Math.ceil(size / BYTES_PER_CHUNK), 1),
                    start = 0, end = BYTES_PER_CHUNK, num = 1, video_name = Date.now()+"."+type.split("/")[1];
                    let formData = new FormData($(this)[0]);
                    let count_chunk_upload = 0;
                    var chunkUpload = function(blob) {
                        var fd = new FormData();

                        fd.append('upload', blob, video_name);
                        fd.append('num', num);
                        fd.append('num_chunks', NUM_CHUNKS);
                        $.ajax({
                            url: '<?php echo site_url('admin/upload_video');?>',
                            type : 'POST',
                            data : fd,
                            cache: false,
                            contentType: false,
                            processData: false ,
                            xhr: function () {
                                var fileXhr = $.ajaxSettings.xhr();
                                if (fileXhr.upload) {
                                    $(".modal #bar").show()
                                    fileXhr.upload.addEventListener("progress", function (e) {
                                        if (e.lengthComputable) {
                                            if(e.loaded == e.total){
                                                count_chunk_upload ++
                                                $('.modal #bar .bar').css('width', `${count_chunk_upload/NUM_CHUNKS * 100}%`);
                                            }
                                        }
                                    }, false);
                                }
                                return fileXhr; 
                            },
                            success: function(response)
                            {
                                if(response){
                                    res = JSON.parse(response);
                                    if(res['video_url']){
                                        formData.append('uploaded_url', res['video_url'])
                                        formData.delete('system_video_file')
                                        $.ajax({
                                            url: url,
                                            type: 'POST',
                                            dataType: 'json',
                                            data: formData,
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            xhr: function () {
                                                var fileXhr = $.ajaxSettings.xhr();
                                                if (fileXhr.upload) {
                                                    fileXhr.upload.addEventListener("progress", function (e) {
                                                        if (e.lengthComputable) {
                                                            if(e.loaded == e.total){
                                                                location.reload();
                                                            } 
                                                        }
                                                    }, false);
                                                }
                                                return fileXhr;
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }

                    while (start < size) {
                        chunkUpload(file.slice(start, end));
                        start = end;
                        end = start + BYTES_PER_CHUNK;
                        num++;
                    }
                    return;
                }
            }
            $.NotificationApp.send("<?php echo get_phrase('oh_snap'); ?>!", '<?php echo get_phrase('please_select_valid_file');?>' ,"top-right","rgba(0,0,0,0.2)","error");
            $(this).find(":submit").removeAttr("disabled");
            return false;
        }
        event.currentTarget.submit()
    });
});

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function ajax_get_video_details(video_url) {
    $('#perloader').show();
    if(checkURLValidity(video_url)){
        $.ajax({
            url: '<?php echo site_url('admin/ajax_get_video_details');?>',
            type : 'POST',
            data : {video_url : video_url},
            success: function(response)
            {
                if (response) {
                    jQuery('#duration').val(response);
                }
                $('#perloader').hide();
                $('#invalid_url').hide();
            }
        });
    }else {
        $('#invalid_url').show();
        $('#perloader').hide();
        jQuery('#duration').val('');

    }
}

function checkURLValidity(video_url) {
    var youtubePregMatch = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    var vimeoPregMatch = /^(http\:\/\/|https\:\/\/)?(www\.)?(vimeo\.com\/)([0-9]+)$/;
    if (video_url.match(youtubePregMatch)) {
        return true;
    }
    else if (vimeoPregMatch.test(video_url)) {
        return true;
    }
    else {
        return false;
    }
}
</script>
